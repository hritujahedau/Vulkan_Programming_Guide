#include <Windows.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <array>

#define VK_USE_PLATFORM_WIN32_KHR 1

#include <vulkan\vulkan.h>

#define WIDTH 1280
#define HEIGHT 720

// windowing functions
void createWindow(HWND& hwnd, HINSTANCE& hInstance);
bool ProccessEvent();
