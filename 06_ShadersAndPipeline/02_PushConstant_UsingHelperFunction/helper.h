#include <Windows.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <array>

#define VK_USE_PLATFORM_WIN32_KHR 1

#include <vulkan\vulkan.h>

#define WIDTH 1280
#define HEIGHT 720

#define min(a,b) (((a) < (b)) ? (a) : (b))

// vulkan helper functions
VkInstance createInstant();
VkSurfaceKHR createSurface(VkInstance instance);
VkSwapchainKHR createSwapchain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, uint32_t queueFamilyIndex);
VkPhysicalDevice enumerateAndGetPhysicalDevice(VkInstance instance, uint32_t index);
uint32_t getQueueFamilyIndex(VkPhysicalDevice physicalDevice, VkQueueFlags requiredQueueFlags);
VkDevice createDevice(VkPhysicalDevice physicalDevice, uint32_t queueFamilyIndex);
uint32_t chooseMemoryType(VkPhysicalDevice physicalDevice, uint32_t memReqType, uint32_t flag);
VkShaderModule createShaderModule(VkDevice device, const std::string& filename);
VkCommandPool createCommandPool(VkDevice device, uint32_t queueFamilyIndex);
VkCommandBuffer createCommandBuffer(VkDevice device, VkCommandPool cmdPool);
VkPipelineLayout createPipelineLayouts(VkDevice device, VkDescriptorSetLayout descSetLayout, VkPushConstantRange pushConstantRange);
VkPipeline createComputePipeline(VkDevice device, VkShaderModule shaderModule, VkPipelineLayout pipelineLayout);

// windowing helper functions
void createWindow(HWND& hwnd, HINSTANCE& hInstance);
bool ProccessEvent();

// other helper functions
std::vector<char> readFile(const std::string& filename);
