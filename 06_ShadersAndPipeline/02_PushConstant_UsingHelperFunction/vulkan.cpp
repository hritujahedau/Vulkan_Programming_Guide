#include "helper.h"

struct my_push_constant_t
    {
        int operation;
    } my_push_constant = {1};

void main()
{
    VkResult vkRes;

    // instance
    VkInstance instance = createInstant();

    // physical device
    VkPhysicalDevice physicalDevice = enumerateAndGetPhysicalDevice(instance, 0);
    uint32_t queueFamilyIndex = getQueueFamilyIndex(physicalDevice, VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT);

    // device
    VkDevice device = createDevice(physicalDevice, queueFamilyIndex);
    VkQueue computeQueue;
    uint32_t queueIndex = 0;
    vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, &computeQueue);
    VkShaderModule shaderModule = createShaderModule(device, "shader.comp.spv");

    // commands
    VkCommandPool cmdPool = createCommandPool(device, queueFamilyIndex);
    VkCommandBuffer cmdBuffer = createCommandBuffer(device, cmdPool);

    VkBuffer buffer1, buffer2, buffer3;
    uint32_t *buffer1Ptr, *buffer2Ptr, *buffer3Ptr;
    {
        uint32_t data[] = {1, 2, 3, 4, 5, 6, 7, 8};
        
        VkBufferCreateInfo bufferCI{};
        bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCI.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        bufferCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufferCI.pQueueFamilyIndices = &queueFamilyIndex;
        bufferCI.queueFamilyIndexCount = 1;
        bufferCI.size = sizeof(data);

        vkRes = vkCreateBuffer(device, &bufferCI, nullptr, &buffer1);
        vkRes = vkCreateBuffer(device, &bufferCI, nullptr, &buffer2);
        vkRes = vkCreateBuffer(device, &bufferCI, nullptr, &buffer3);

        VkMemoryRequirements memReq;

        vkGetBufferMemoryRequirements(device, buffer1, &memReq);

        VkMemoryAllocateInfo memAllocInfo{};
        memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        memAllocInfo.allocationSize = memReq.size;
        memAllocInfo.memoryTypeIndex = chooseMemoryType(physicalDevice, memReq.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

        VkDeviceMemory buffer1DeviceMemory, buffer2DeviceMemory, buffer3DeviceMemory;
        vkAllocateMemory(device, &memAllocInfo, nullptr, &buffer1DeviceMemory);
        vkAllocateMemory(device, &memAllocInfo, nullptr, &buffer2DeviceMemory);
        vkAllocateMemory(device, &memAllocInfo, nullptr, &buffer3DeviceMemory);

        vkBindBufferMemory(device, buffer1, buffer1DeviceMemory, 0);
        vkBindBufferMemory(device, buffer2, buffer2DeviceMemory, 0);
        vkBindBufferMemory(device, buffer3, buffer3DeviceMemory, 0);
        
        vkMapMemory(device, buffer1DeviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)&buffer1Ptr);
        vkMapMemory(device, buffer2DeviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)&buffer2Ptr);
        vkMapMemory(device, buffer3DeviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)&buffer3Ptr);

        memcpy(buffer1Ptr, data, sizeof(data));
        memcpy(buffer2Ptr, data, sizeof(data));
    }

    VkDescriptorPoolSize descPoolSizes{};
    descPoolSizes.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descPoolSizes.descriptorCount = 3;

    VkDescriptorPool descPool;
    VkDescriptorPoolCreateInfo descPoolCI{};
    descPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descPoolCI.poolSizeCount = 1;
    descPoolCI.pPoolSizes = &descPoolSizes;
    descPoolCI.maxSets = 1;

    vkRes = vkCreateDescriptorPool(device, &descPoolCI, nullptr, &descPool);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateDescriptorPool" << std::endl;
    }

    std::array<VkDescriptorSetLayoutBinding, 3> descSetLayoutBinding{};

    for(int i = 0; i < 3; i++)
    {
        descSetLayoutBinding[i].binding = i;
        descSetLayoutBinding[i].descriptorCount = 1;
        descSetLayoutBinding[i].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descSetLayoutBinding[i].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    }    

    VkDescriptorSetLayout descSetLayout;
    VkDescriptorSetLayoutCreateInfo descSetLayoutCI{};
    descSetLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descSetLayoutCI.bindingCount = descSetLayoutBinding.size();
    descSetLayoutCI.pBindings = descSetLayoutBinding.data();

    vkRes = vkCreateDescriptorSetLayout(device, &descSetLayoutCI, nullptr, &descSetLayout);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateDescriptorSetLayout" << std::endl;
    }

    VkDescriptorSet descSet;
    VkDescriptorSetAllocateInfo descSetAI{};
    descSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descSetAI.descriptorPool = descPool;
    descSetAI.descriptorSetCount = 1;
    descSetAI.pSetLayouts = &descSetLayout;

    vkAllocateDescriptorSets(device, &descSetAI, &descSet);

    // pipeline
    VkPushConstantRange pushConstantRange{};
    pushConstantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    pushConstantRange.offset = 0;
    pushConstantRange.size = sizeof(my_push_constant_t);

    VkPipelineLayout pipelineLayout = createPipelineLayouts(device, descSetLayout, pushConstantRange);
    VkPipeline computePipeline = createComputePipeline(device, shaderModule, pipelineLayout);

    std::array<VkBuffer, 3> buffers{buffer1, buffer2, buffer3};
    std::array<VkDescriptorBufferInfo, 3> descBufferInfo{};
    std::array<VkWriteDescriptorSet,3> writeDescSet{};

    for(int i = 0; i < 3; i ++)
    {        
        descBufferInfo[i].buffer = buffers[i];
        descBufferInfo[i].offset = 0;
        descBufferInfo[i].range = VK_WHOLE_SIZE;
        
        writeDescSet[i].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescSet[i].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writeDescSet[i].descriptorCount = 1;
        writeDescSet[i].dstSet = descSet;
        writeDescSet[i].dstBinding = i;
        writeDescSet[i].pBufferInfo = &descBufferInfo[i];
    }
    
    vkUpdateDescriptorSets(device, writeDescSet.size(), writeDescSet.data(), 0, nullptr);

    VkCommandBufferBeginInfo cmdBufferBeginInfo{};
    cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    VkSubmitInfo submitInfo{};

    {
        vkBeginCommandBuffer(cmdBuffer, &cmdBufferBeginInfo);
        vkCmdPushConstants(cmdBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(my_push_constant), &my_push_constant);
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline);
        vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout,0, 1, &descSet, 0, nullptr );
        vkCmdDispatch(cmdBuffer, 8/4, 1, 1);
        vkEndCommandBuffer(cmdBuffer);

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &cmdBuffer;

        vkQueueSubmit(computeQueue, 1, &submitInfo, VK_NULL_HANDLE);

        while(vkQueueWaitIdle(computeQueue) != VK_SUCCESS);

        std::cout<<"Addition:"<<std::endl;
        for(int i = 0; i < 8; i++)
        {
            std::cout<<i<< " " << buffer1Ptr[i] << " " << buffer2Ptr[i]<< " " << buffer3Ptr[i] << std::endl;
        }

        vkResetCommandPool(device, cmdPool, 0);
    }
}
