#include "helper.h"

//namespace myvk
//{
    VkInstance createInstant()
    {
        VkInstance instance;
        VkInstanceCreateInfo instanceCI{};
        VkApplicationInfo appInfo{};

        char* layersNames = "VK_LAYER_KHRONOS_validation";

        std::vector<const char*> extensionList {
                VK_KHR_SURFACE_EXTENSION_NAME,
                VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
            };

        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.apiVersion = VK_VERSION_1_0;
        appInfo.engineVersion = 1.0;
        appInfo.pApplicationName = "Hrituja Vulkan program";
        appInfo.applicationVersion = 1;

        instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceCI.pApplicationInfo = &appInfo;
        instanceCI.enabledLayerCount = 1;
        instanceCI.ppEnabledLayerNames = &layersNames;
        instanceCI.enabledExtensionCount = extensionList.size();
        instanceCI.ppEnabledExtensionNames = extensionList.data();
        
        VkResult vkRes = vkCreateInstance(&instanceCI, nullptr, &instance);
        if(vkRes != VK_SUCCESS)
        {
            std::cout<<"Failed: vkCreateInstance" << std::endl;
            exit(ERROR);
        }

        return instance;
    }

    VkPhysicalDevice enumerateAndGetPhysicalDevice(VkInstance instance, uint32_t index)
    {
        std::vector<VkPhysicalDevice> physicalDeviceVec;
        uint32_t physicalDeviceCount = 0;
        vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, nullptr);

        physicalDeviceVec.resize(physicalDeviceCount);

        vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDeviceVec.data());

        return physicalDeviceVec[0];
    }

    uint32_t getQueueFamilyIndex(VkPhysicalDevice physicalDevice, VkQueueFlags requiredQueueFlags)
    {
        uint32_t queueFamilyIndex, queueFamilyCount;
        std::vector<VkQueueFamilyProperties> queueFamilyProp{};

        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
        queueFamilyProp.resize(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyProp.data());

        for(int i= 0 ; i < queueFamilyCount; i++)
        {
            if(queueFamilyProp[i].queueFlags & requiredQueueFlags)
            {
                queueFamilyIndex = i;
                break;
            }
        }
        return queueFamilyIndex;
    }

    VkDevice createDevice(VkPhysicalDevice physicalDevice, uint32_t queueFamilyIndex )
    {
        VkDevice device;

        float queuePrio = 1.0;

        VkDeviceQueueCreateInfo deviceQueueCI{};
        deviceQueueCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCI.queueFamilyIndex = queueFamilyIndex;
        deviceQueueCI.queueCount = 1;
        deviceQueueCI.pQueuePriorities = &queuePrio;

        std::vector<const char*> extensions {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };

        VkDeviceCreateInfo deviceCI{};
        deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCI.queueCreateInfoCount = 1;
        deviceCI.pQueueCreateInfos = &deviceQueueCI;
        deviceCI.enabledExtensionCount = extensions.size();
        deviceCI.ppEnabledExtensionNames = extensions.data();

        VkResult vkRes = vkCreateDevice(physicalDevice, &deviceCI, nullptr, &device);
        if(vkRes != VK_SUCCESS)
        {
            std::cout<<"Failed: vkCreateDevice" << std::endl;
            exit(ERROR);
        }

        return device;
    }

    VkShaderModule createShaderModule(VkDevice device, const std::string& filename)
    {
        std::vector<char> shaderCode = readFile(filename);

        VkShaderModule shaderModule;
        VkShaderModuleCreateInfo shaderModuleCI{};
        shaderModuleCI.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        shaderModuleCI.codeSize = shaderCode.size();
        shaderModuleCI.pCode = reinterpret_cast<uint32_t*>(shaderCode.data());

        VkResult vkRes = vkCreateShaderModule(device, &shaderModuleCI, nullptr, &shaderModule);
        if(vkRes != VK_SUCCESS)
        {
            std::cout<<"Failed: vkCreateShaderModule" << std::endl;
            exit(ERROR);
        }

        return shaderModule;
    }

    VkCommandPool createCommandPool(VkDevice device, uint32_t queueFamilyIndex)
    {
        VkCommandPool cmdPool;
        VkCommandPoolCreateInfo cmdPoolCI{};
        cmdPoolCI.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        cmdPoolCI.queueFamilyIndex = queueFamilyIndex;
        
        VkResult vkRes = vkCreateCommandPool(device, &cmdPoolCI, nullptr, &cmdPool);
        if(vkRes != VK_SUCCESS)
        {
            std::cout<<"Failed: vkCreateCommandPool" << std::endl;
            exit(ERROR);
        }
        return cmdPool;
    }

    VkCommandBuffer createCommandBuffer(VkDevice device, VkCommandPool cmdPool)
    {
        VkCommandBuffer cmdBuffer;
        VkCommandBufferAllocateInfo cmdBufferAI{};
        cmdBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cmdBufferAI.commandPool = cmdPool;
        cmdBufferAI.commandBufferCount = 1;
        cmdBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

        vkAllocateCommandBuffers(device, &cmdBufferAI, &cmdBuffer);

        return cmdBuffer;
    }

    VkPipelineLayout createPipelineLayouts(VkDevice device, VkDescriptorSetLayout descSetLayout, VkPushConstantRange pushConstantRange)
    {
        VkPipelineLayout pipelineLayout{};
        VkPipelineLayoutCreateInfo layoutCI{};
        layoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        layoutCI.setLayoutCount = 1;
        layoutCI.pSetLayouts = &descSetLayout;
        layoutCI.pushConstantRangeCount = 1;
        layoutCI.pPushConstantRanges = &pushConstantRange;

        VkResult vkRes = vkCreatePipelineLayout(device, &layoutCI, nullptr, &pipelineLayout);
        if(vkRes != VK_SUCCESS)
        {
            std::cout<<"Failed: vkCreatePipelineLayout" << std::endl;
            exit(ERROR);
        }
        return pipelineLayout;
    }

    VkPipeline createComputePipeline(VkDevice device, VkShaderModule shaderModule, VkPipelineLayout pipelineLayout)
    {
        VkPipeline computePipeline;

        VkPipelineShaderStageCreateInfo pipelineShaderStageCI{};
        pipelineShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        pipelineShaderStageCI.module = shaderModule;
        pipelineShaderStageCI.pName = "main";
        pipelineShaderStageCI.stage = VK_SHADER_STAGE_COMPUTE_BIT;

        VkComputePipelineCreateInfo computePipelineCI{};
        computePipelineCI.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        computePipelineCI.layout = pipelineLayout;
        computePipelineCI.stage = pipelineShaderStageCI;

        VkResult vkRes = vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &computePipelineCI, nullptr, &computePipeline);
        if(vkRes != VK_SUCCESS)
        {
            std::cout<<"Failed: vkCreatePipelineLayout" << std::endl;
            exit(ERROR);
        }

        return computePipeline;
    }

    VkSurfaceKHR createSurface(VkInstance instance)
    {
        HWND hwnd;
        HINSTANCE hInstance;
        createWindow(hwnd, hInstance);

        VkWin32SurfaceCreateInfoKHR surfaceCreateInfo{};
        surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
        surfaceCreateInfo.flags =  0;
        surfaceCreateInfo.hwnd = hwnd;
        surfaceCreateInfo.hinstance = hInstance;

        VkSurfaceKHR surface;
        VkResult vkRes = vkCreateWin32SurfaceKHR(instance, &surfaceCreateInfo, nullptr, &surface);
        if (vkRes != VK_SUCCESS)
        {
            std::cout << "Failed: vkCreateWin32SurfaceKHR";
            exit(ERROR);
        }
        return surface;
    }

    VkSwapchainKHR createSwapchain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, uint32_t queueFamilyIndex)
    {
        VkSwapchainKHR swapChain;
        VkSwapchainCreateInfoKHR swapChainCreateInfo{};

        VkSurfaceCapabilitiesKHR surfaceCapabilities{};

        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);

        swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        swapChainCreateInfo.surface = surface;
        swapChainCreateInfo.minImageCount = min((surfaceCapabilities.minImageCount + 1), surfaceCapabilities.maxImageCount);
        
        uint32_t surfaceFormatCount;
        std::vector<VkSurfaceFormatKHR> surfaceFormat;
        vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, nullptr);

        surfaceFormat.resize(surfaceFormatCount);

        vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, surfaceFormat.data());

        VkBool32 bQueueFamilyIndexSurfaceSupport;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, &bQueueFamilyIndexSurfaceSupport);
        // selected queue family index has support
        if(!bQueueFamilyIndexSurfaceSupport)
        {
        	printf("\nFail: Queue Family Index Surface Not Support");
            exit(ERROR);
        }

        printf("\nQueue Family Index Surface Support");
        
        swapChainCreateInfo.imageFormat = surfaceFormat[0].format;
        swapChainCreateInfo.imageColorSpace = surfaceFormat[0].colorSpace; //at present state only defined color space is VK_COLORSPACE_SRGB_NONLINEAR_KHR;
        swapChainCreateInfo.imageExtent = surfaceCapabilities.minImageExtent;
        swapChainCreateInfo.imageArrayLayers = 1;
        swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
        swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swapChainCreateInfo.queueFamilyIndexCount = 1;
        swapChainCreateInfo.pQueueFamilyIndices = &queueFamilyIndex;
        swapChainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        swapChainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
        swapChainCreateInfo.clipped = VK_TRUE;
        swapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

        VkResult vkRes = vkCreateSwapchainKHR(device, &swapChainCreateInfo, nullptr, &swapChain);
        if (vkRes != VK_SUCCESS) 
        {
            printf("Failed: vkCreateSwapchainKHR()");
            exit(ERROR);
        }

        printf("\nvkCreateSwapchainKHR()");

    return swapChain;
    }

    uint32_t chooseMemoryType(VkPhysicalDevice physicalDevice, uint32_t memReqType, uint32_t flag)
    {
        VkPhysicalDeviceMemoryProperties memProp;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProp);

        uint32_t selectedType = ~0u;
        uint32_t memTypeBit = 0 ;

        for(; memTypeBit <  32; memTypeBit ++)
        {
            if((1 << memTypeBit) & memReqType) // first check with if is compatible with buffers reuired mem type
            {
                const VkMemoryType& memType = memProp.memoryTypes[memTypeBit];
                if((memType.propertyFlags & flag) == flag)
                {
                    selectedType = memTypeBit;
                    break;
                }
            }
    }

    return selectedType;
    }
//}