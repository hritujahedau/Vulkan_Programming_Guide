#include "helper.h"

struct push_constant_t
{
    float cx;
    float cy;
} push_constant {0.285, 0.0} ;

void main()
{
    VkInstance instance = createInstant();
    VkPhysicalDevice physicalDevice = enumerateAndGetPhysicalDevice(instance);
    uint32_t queueFamilyIndex = getQueueFamilyIndex(physicalDevice, VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT);
    uint32_t queueIndex = 0;
    VkDevice device = createDevice(physicalDevice, queueFamilyIndex);

    VkQueue queue ;
    vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, &queue);

    VkSurfaceKHR  surface = createSurface(instance);
    VkSwapchainKHR swapchain = createSwapchain(physicalDevice, device, surface, queueFamilyIndex);

    VkCommandPool cmdPool = createCommandPool(device, queueFamilyIndex);
    VkCommandBuffer cmdBuffer = createCommandBuffer(device, cmdPool);

    VkShaderModule shaderModule = createShaderModule(device, "shader.comp.spv");
    
    std::vector<VkImage> swapchainImages = getSwapChainImages(device, swapchain);
    std::vector<VkImageView> swapchainImageView(swapchainImages.size());
    VkImageViewCreateInfo imgViewCI{};
    VkImageSubresourceRange imgSubRR{};

    imgSubRR.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imgSubRR.baseArrayLayer = 0;
    imgSubRR.baseMipLevel = 0;
    imgSubRR.layerCount = 1;
    imgSubRR.levelCount = 1;

    imgViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imgViewCI.format = VK_FORMAT_B8G8R8A8_UNORM;
    imgViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imgViewCI.subresourceRange = imgSubRR;

    for(int i = 0; i < swapchainImages.size(); i++)
    {
        imgViewCI.image = swapchainImages[i];
        vkCreateImageView(device, &imgViewCI, nullptr, &swapchainImageView[i]);
    }

    // descriptors
    VkDescriptorPoolSize descPoolSizes{};
    descPoolSizes.type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    descPoolSizes.descriptorCount = swapchainImages.size();

    VkDescriptorPool descPool;
    VkDescriptorPoolCreateInfo descPoolCI{};

    descPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descPoolCI.pPoolSizes = &descPoolSizes;
    descPoolCI.poolSizeCount = 1;
    descPoolCI.maxSets = swapchainImages.size();

    vkCreateDescriptorPool(device, &descPoolCI, nullptr, &descPool);

    VkDescriptorSetLayoutBinding descSetLayoutBinding{};
    descSetLayoutBinding.binding = 0;
    descSetLayoutBinding.descriptorCount = 1;
    descSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    descSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

    VkDescriptorSetLayout descSetLayout{};
    VkDescriptorSetLayoutCreateInfo descSetLayoutCI{};

    descSetLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descSetLayoutCI.bindingCount = 1;
    descSetLayoutCI.pBindings = &descSetLayoutBinding;
    
    vkCreateDescriptorSetLayout(device, &descSetLayoutCI, nullptr, &descSetLayout);

    std::vector<VkDescriptorSet> descSets(swapchainImages.size());
    VkDescriptorSetAllocateInfo descSetAI{};
    std::vector<VkDescriptorSetLayout> descSetLayouts(swapchainImages.size(), descSetLayout);

    descSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descSetAI.descriptorPool = descPool;
    descSetAI.descriptorSetCount = descSetLayouts.size();
    descSetAI.pSetLayouts = descSetLayouts.data();
    
    vkAllocateDescriptorSets(device, &descSetAI, descSets.data());

    VkPushConstantRange pushConstantRange{};
    pushConstantRange.size =sizeof(VkPushConstantRange);
    pushConstantRange.offset = 0;
    pushConstantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

    VkPipelineLayout pipelineLayout;
    VkPipelineLayoutCreateInfo pipelineLayoutCI{};

    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCI.setLayoutCount = 1;
    pipelineLayoutCI.pSetLayouts = &descSetLayout;
    pipelineLayoutCI.pushConstantRangeCount = 1;
    pipelineLayoutCI.pPushConstantRanges = &pushConstantRange;

    vkCreatePipelineLayout(device, &pipelineLayoutCI, nullptr, &pipelineLayout);

    // for mandelbrot
    int value = 0;
    VkSpecializationMapEntry specMapEntry{};
    VkSpecializationInfo specInfo;
    VkPipeline pipeline_Madelbrot;
    VkPipeline pipeline_JuliaSet;

    {
        value = 1;
        specMapEntry.constantID = 0;
        specMapEntry.offset = 0;
        specMapEntry.size = sizeof(int);

        specInfo.pMapEntries = &specMapEntry;
        specInfo.mapEntryCount = 1;
        specInfo.pData = &value;
        specInfo.dataSize = sizeof(int);

        pipeline_Madelbrot = createComputePipeline(device, shaderModule, pipelineLayout, specInfo);
    }
    {
        value = 2;
        specMapEntry.constantID = 0;
        specMapEntry.offset = 0;
        specMapEntry.size = sizeof(int);

        specInfo.pMapEntries = &specMapEntry;
        specInfo.mapEntryCount = 1;
        specInfo.pData = &value;
        specInfo.dataSize = sizeof(int);

        pipeline_JuliaSet = createComputePipeline(device, shaderModule, pipelineLayout, specInfo);
    }

    VkPipeline activePipleine = pipeline_JuliaSet;

    VkImageMemoryBarrier imgMemoryBarrier{};
    imgMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;

    VkDescriptorImageInfo descImgInfo{};
    descImgInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
    

    VkWriteDescriptorSet writeDescSet{};
    writeDescSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescSet.dstBinding = 0;
    writeDescSet.descriptorCount = 1;
    
    writeDescSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    
    writeDescSet.pImageInfo = &descImgInfo;

    for(int i =0; i < descSets.size(); i++)
    {
        descImgInfo.imageView = swapchainImageView[i];
        writeDescSet.dstSet = descSets[i];

        vkUpdateDescriptorSets(device, 1, &writeDescSet, 0, nullptr);
    }  

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkCommandBufferBeginInfo cmdBufferBI{};
    cmdBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    VkPresentInfoKHR present{};
    present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;    

    bool bExit = false;
    float r = 0.0f;
    for(int i = 0; bExit != true ;i ++) 
    {
        uint32_t imgIndex = aquireNextImage(device, swapchain);    
        const VkImage& image = swapchainImages[imgIndex];
        imgMemoryBarrier.dstQueueFamilyIndex = queueFamilyIndex;
        imgMemoryBarrier.image = image;
        imgMemoryBarrier.subresourceRange = imgSubRR;
        {
            push_constant.cx = (sinf(r* 0.2f) + (cosf(r * 0.33))) * 0.5;
            push_constant.cy = (sinf(r* 0.23f) + (cosf(r * 0.31))) * 0.5;
            r += 0.01f;

            vkBeginCommandBuffer(cmdBuffer,&cmdBufferBI);     
            imgMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            imgMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
            imgMemoryBarrier.srcAccessMask = VK_ACCESS_NONE;
            imgMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
            vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &imgMemoryBarrier);
            vkCmdPushConstants(cmdBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(push_constant_t), &push_constant);
            //printf("\npush_constant x = %f, y = %f", push_constant.cx, push_constant.cy);
            vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, activePipleine);
            vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descSets[imgIndex], 0, nullptr);
            vkCmdDispatch(cmdBuffer, WIDTH/32, HEIGHT/32, 1);
        }

        // for present image
        {
            imgMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
            imgMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            imgMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
            imgMemoryBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
            vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &imgMemoryBarrier);

            vkEndCommandBuffer(cmdBuffer); 

            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &cmdBuffer;
            
            vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
            vkQueueWaitIdle(queue);

            present.swapchainCount = 1;
            present.pSwapchains = &swapchain;
            present.pImageIndices = &imgIndex;
            
            vkQueuePresentKHR(queue, &present);

            vkQueueWaitIdle(queue);

            vkResetCommandPool(device, cmdPool, 0);

        }

        if(ProccessEvent() == true)
        {
            bExit = true;
        }
    }

    

}
