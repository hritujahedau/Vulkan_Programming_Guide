#include "helper.h"

std::vector<char> readFile(const std::string& filename)
{
		std::ifstream file(filename, std::ios::ate | std::ios::binary);

		if (!file.is_open())
		{
			throw std::runtime_error("error: cannot open shader file");
		}

		size_t filesize = static_cast<size_t>(file.tellg());
		std::vector<char> buffer(filesize);

		file.seekg(0, std::ios::beg);
		file.read(buffer.data(), filesize);
		file.close();

		return buffer;
}


