#include "helper.h"

uint32_t ChooseMemoryType(VkPhysicalDevice physicalDevice, uint32_t memReqType, uint32_t flag)
{
    VkPhysicalDeviceMemoryProperties memProp;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProp);

    uint32_t selectedType = ~0u;
    uint32_t memTypeBit = 0 ;

    for(; memTypeBit <  32; memTypeBit ++)
    {
        if((1 << memTypeBit) & memReqType) // first check with if is compatible with buffers reuired mem type
        {
            const VkMemoryType& memType = memProp.memoryTypes[memTypeBit];
            if((memType.propertyFlags & flag) == flag)
            {
                selectedType = memTypeBit;
                break;
            }
        }
    }

    return selectedType;

}

std::vector<char> readFile(const std::string& filename)
{
		std::ifstream file(filename, std::ios::ate | std::ios::binary);

		if (!file.is_open())
		{
			throw std::runtime_error("error: cannot open shader file");
		}

		size_t filesize = static_cast<size_t>(file.tellg());
		std::vector<char> buffer(filesize);

		file.seekg(0, std::ios::beg);
		file.read(buffer.data(), filesize);
		file.close();

		return buffer;
}


void main()
{
    VkResult vkRes;

    VkInstance instance;
    VkInstanceCreateInfo instanceCI{};
    VkApplicationInfo appInfo{};

    char* layersNames = "VK_LAYER_KHRONOS_validation";

    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.apiVersion = VK_VERSION_1_0;
    appInfo.engineVersion = 1.0;
    appInfo.pApplicationName = "Hrituja Vulkan program";

    instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCI.pApplicationInfo = &appInfo;
    instanceCI.enabledLayerCount = 1;
    instanceCI.ppEnabledLayerNames = &layersNames;
    
    vkRes = vkCreateInstance(&instanceCI, nullptr, &instance);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateInstance" << std::endl;
    }

    std::vector<VkPhysicalDevice> physicalDeviceVec;
    VkPhysicalDevice physicalDevice;
    uint32_t physicalDeviceCount = 0;
    vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, nullptr);

    physicalDeviceVec.resize(physicalDeviceCount);

    vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDeviceVec.data());

    physicalDevice = physicalDeviceVec[0];

    uint32_t queueFamilyIndex, queueFamilyCount, queueIndex = 0;
    std::vector<VkQueueFamilyProperties> queueFamilyProp{};

    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
    queueFamilyProp.resize(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyProp.data());

    for(int i= 0 ; i < queueFamilyCount; i++)
    {
        if(queueFamilyProp[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
        {
            queueFamilyIndex = i;
        }
    }

    float queuePrio = 1.0;

    VkDeviceQueueCreateInfo deviceQueueCI{};
    deviceQueueCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    deviceQueueCI.queueFamilyIndex = queueFamilyIndex;
    deviceQueueCI.queueCount = 1;
    deviceQueueCI.pQueuePriorities = &queuePrio;

    VkDeviceCreateInfo deviceCI{};
    deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCI.queueCreateInfoCount = 1;
    deviceCI.pQueueCreateInfos = &deviceQueueCI;
    
    VkDevice device;

    vkRes = vkCreateDevice(physicalDevice, &deviceCI, nullptr, &device);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateDevice" << std::endl;
    }

    VkQueue computeQueue;
    vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, &computeQueue);
    
    uint32_t data[] = {1, 2, 3, 4, 5, 6, 7, 8};

    VkBuffer buffer1, buffer2, buffer3;
    VkBufferCreateInfo bufferCI{};
    bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCI.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    bufferCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferCI.pQueueFamilyIndices = &queueFamilyIndex;
    bufferCI.queueFamilyIndexCount = 1;
    bufferCI.size = sizeof(data);

    vkRes = vkCreateBuffer(device, &bufferCI, nullptr, &buffer1);
    vkRes = vkCreateBuffer(device, &bufferCI, nullptr, &buffer2);
    vkRes = vkCreateBuffer(device, &bufferCI, nullptr, &buffer3);

    VkMemoryRequirements memReq;

    vkGetBufferMemoryRequirements(device, buffer1, &memReq);

    VkMemoryAllocateInfo memAllocInfo{};
    memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memAllocInfo.allocationSize = memReq.size;
    memAllocInfo.memoryTypeIndex = ChooseMemoryType(physicalDevice, memReq.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    VkDeviceMemory buffer1DeviceMemory, buffer2DeviceMemory, buffer3DeviceMemory;
    vkAllocateMemory(device, &memAllocInfo, nullptr, &buffer1DeviceMemory);
    vkAllocateMemory(device, &memAllocInfo, nullptr, &buffer2DeviceMemory);
    vkAllocateMemory(device, &memAllocInfo, nullptr, &buffer3DeviceMemory);

    vkBindBufferMemory(device, buffer1, buffer1DeviceMemory, 0);
    vkBindBufferMemory(device, buffer2, buffer2DeviceMemory, 0);
    vkBindBufferMemory(device, buffer3, buffer3DeviceMemory, 0);

    uint32_t *buffer1Ptr, *buffer2Ptr, *buffer3Ptr;
    vkMapMemory(device, buffer1DeviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)&buffer1Ptr);
    vkMapMemory(device, buffer2DeviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)&buffer2Ptr);
    vkMapMemory(device, buffer3DeviceMemory, 0, VK_WHOLE_SIZE, 0, (void**)&buffer3Ptr);

    memcpy(buffer1Ptr, data, sizeof(data));
    memcpy(buffer2Ptr, data, sizeof(data));

    VkDescriptorPoolSize descPoolSizes{};
    descPoolSizes.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descPoolSizes.descriptorCount = 3;

    VkDescriptorPool descPool;
    VkDescriptorPoolCreateInfo descPoolCI{};
    descPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descPoolCI.poolSizeCount = 1;
    descPoolCI.pPoolSizes = &descPoolSizes;
    descPoolCI.maxSets = 1;

    vkRes = vkCreateDescriptorPool(device, &descPoolCI, nullptr, &descPool);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateDescriptorPool" << std::endl;
    }

    std::array<VkDescriptorSetLayoutBinding, 3> descSetLayoutBinding{};

    for(int i = 0; i < 3; i++)
    {
        descSetLayoutBinding[i].binding = i;
        descSetLayoutBinding[i].descriptorCount = 1;
        descSetLayoutBinding[i].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descSetLayoutBinding[i].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    }    

    VkDescriptorSetLayout descSetLayout;
    VkDescriptorSetLayoutCreateInfo descSetLayoutCI{};
    descSetLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descSetLayoutCI.bindingCount = descSetLayoutBinding.size();
    descSetLayoutCI.pBindings = descSetLayoutBinding.data();

    vkRes = vkCreateDescriptorSetLayout(device, &descSetLayoutCI, nullptr, &descSetLayout);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateDescriptorSetLayout" << std::endl;
    }

    VkDescriptorSet descSet;
    VkDescriptorSetAllocateInfo descSetAI{};
    descSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descSetAI.descriptorPool = descPool;
    descSetAI.descriptorSetCount = 1;
    descSetAI.pSetLayouts = &descSetLayout;

    vkAllocateDescriptorSets(device, &descSetAI, &descSet);

    std::vector<char> shaderCode = readFile("shader.comp.spv");

    VkShaderModule shaderModule;
    VkShaderModuleCreateInfo shaderModuleCI{};
    shaderModuleCI.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderModuleCI.codeSize = shaderCode.size();
    shaderModuleCI.pCode = reinterpret_cast<uint32_t*>(shaderCode.data());

    vkRes = vkCreateShaderModule(device, &shaderModuleCI, nullptr, &shaderModule);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateShaderModule" << std::endl;
    }

    VkCommandPool cmdPool;
    VkCommandPoolCreateInfo cmdPoolCI{};
    cmdPoolCI.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmdPoolCI.queueFamilyIndex = queueFamilyIndex;
    
    vkRes = vkCreateCommandPool(device, &cmdPoolCI, nullptr, &cmdPool);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateCommandPool" << std::endl;
    }

    VkCommandBuffer cmdBuffer;
    VkCommandBufferAllocateInfo cmdBufferAI{};
    cmdBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmdBufferAI.commandPool = cmdPool;
    cmdBufferAI.commandBufferCount = 1;
    cmdBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

    vkAllocateCommandBuffers(device, &cmdBufferAI, &cmdBuffer);

    VkPipelineLayout pipelineLayout{};
    VkPipelineLayoutCreateInfo layoutCI{};
    layoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    layoutCI.setLayoutCount = 1;
    layoutCI.pSetLayouts = &descSetLayout;

    vkRes = vkCreatePipelineLayout(device, &layoutCI, nullptr, &pipelineLayout);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreatePipelineLayout" << std::endl;
    }

    VkPipeline computePipeline;

    VkPipelineShaderStageCreateInfo pipelineShaderStageCI{};
    pipelineShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineShaderStageCI.module = shaderModule;
    pipelineShaderStageCI.pName = "main";
    pipelineShaderStageCI.stage = VK_SHADER_STAGE_COMPUTE_BIT;

    VkComputePipelineCreateInfo computePipelineCI{};
    computePipelineCI.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    computePipelineCI.layout = pipelineLayout;
    computePipelineCI.stage = pipelineShaderStageCI;

    vkRes = vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &computePipelineCI, nullptr, &computePipeline);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreatePipelineLayout" << std::endl;
    }

    std::array<VkBuffer, 3> buffers{buffer1, buffer2, buffer3};
    std::array<VkDescriptorBufferInfo, 3> descBufferInfo{};
    std::array<VkWriteDescriptorSet,3> writeDescSet{};

    for(int i = 0; i < 3; i ++)
    {        
        descBufferInfo[i].buffer = buffers[i];
        descBufferInfo[i].offset = 0;
        descBufferInfo[i].range = VK_WHOLE_SIZE;
        
        writeDescSet[i].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescSet[i].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        writeDescSet[i].descriptorCount = 1;
        writeDescSet[i].dstSet = descSet;
        writeDescSet[i].dstBinding = i;
        writeDescSet[i].pBufferInfo = &descBufferInfo[i];
    }
    
    vkUpdateDescriptorSets(device, writeDescSet.size(), writeDescSet.data(), 0, nullptr);

    VkCommandBufferBeginInfo cmdBufferBeginInfo{};
    cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    vkBeginCommandBuffer(cmdBuffer, &cmdBufferBeginInfo);
    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline);
    vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout,0, 1, &descSet, 0, nullptr );
    vkCmdDispatch(cmdBuffer, 8/4, 1, 1);
    vkEndCommandBuffer(cmdBuffer);

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmdBuffer;

    vkQueueSubmit(computeQueue, 1, &submitInfo, VK_NULL_HANDLE);

    while(vkQueueWaitIdle(computeQueue) != VK_SUCCESS);
    
    for(int i = 0; i < 8; i++)
    {
        std::cout<<i<< " " << buffer1Ptr[i] << " " << buffer2Ptr[i]<< " " << buffer3Ptr[i] << std::endl;
    }

}
