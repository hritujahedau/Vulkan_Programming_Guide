#include "helper.h"

struct my_push_constant_t
    {
        int width;
        int height;
    } my_push_constant = {WIDTH, HEIGHT};

void main()
{
    VkResult vkRes;

    // instance
    VkInstance instance = createInstant();
    VkSurfaceKHR surface = createSurface(instance);

    // physical device
    VkPhysicalDevice physicalDevice = enumerateAndGetPhysicalDevice(instance, 0);
    uint32_t queueFamilyIndex = getQueueFamilyIndex(physicalDevice, VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT);

    // device
    VkDevice device = createDevice(physicalDevice, queueFamilyIndex);
    VkQueue queue;
    uint32_t queueIndex = 0;
    vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, &queue);
    VkShaderModule shaderModule = createShaderModule(device, "shader.comp.spv");

    VkSwapchainKHR swapchain = createSwapchain(physicalDevice, device, surface, queueFamilyIndex);
    std::vector<VkImage> swapchainImages = getSwapChainImages(device, swapchain);
    uint32_t swapchainImageIndex = aquireNextImage(device, swapchain);

    // commands
    VkCommandPool cmdPool = createCommandPool(device, queueFamilyIndex);
    VkCommandBuffer cmdBuffer = createCommandBuffer(device, cmdPool);

    VkImage image = swapchainImages[swapchainImageIndex];
    VkDeviceMemory imageDeviceMemory;
    uint32_t imageMapPtr;
    VkImageView imageView;
    VkImageViewCreateInfo imageViewCI{};

    VkImageSubresourceRange imgSubRR;
    imgSubRR.aspectMask =VK_IMAGE_ASPECT_COLOR_BIT;
    imgSubRR.baseArrayLayer = 0;
    imgSubRR.baseMipLevel = 0;
    imgSubRR.layerCount = 1;
    imgSubRR.levelCount = 1;

    imageViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewCI.image = image;
    imageViewCI.format = VK_FORMAT_B8G8R8A8_UNORM;
    imageViewCI.subresourceRange = imgSubRR;
    imageViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;

    vkCreateImageView(device, &imageViewCI, nullptr, &imageView);

    //create image
    // {
    //     VkImageCreateInfo imageCI{};
    //     imageCI.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    //     imageCI.imageType = VK_IMAGE_TYPE_2D;
    //     imageCI.pQueueFamilyIndices = &queueFamilyIndex;
    //     imageCI.queueFamilyIndexCount = 1;
    //     imageCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    //     imageCI.usage = VK_IMAGE_USAGE_STORAGE_BIT;
    //     imageCI.mipLevels = 1;
    //     imageCI.format = VK_FORMAT_R8G8B8A8_UNORM;
    //     imageCI.extent = {WIDTH, HEIGHT, 1};
    //     imageCI.tiling = VK_IMAGE_TILING_LINEAR;
    //     imageCI.initialLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    //     imageCI.samples = VK_SAMPLE_COUNT_1_BIT;
    //     imageCI.arrayLayers = 1;
    //     imageCI.flags = 0;

    //     vkCreateImage(device, &imageCI, nullptr, &image);

    //     imgSubRR.aspectMask =VK_IMAGE_ASPECT_COLOR_BIT;
    //     imgSubRR.baseArrayLayer = 1;
    //     imgSubRR.baseMipLevel = 1;
    //     imgSubRR.layerCount = 1;
    //     imgSubRR.levelCount = 1;

    //     VkImageViewCreateInfo imageViewCI{};
    //     imageViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    //     imageViewCI.image = image;
    //     imageViewCI.format = VK_FORMAT_R8G8B8A8_UNORM;
    //     imageViewCI.subresourceRange = imgSubRR;

    //     vkCreateImageView(device, &imageViewCI, nullptr, &imageView);

    //     VkMemoryRequirements memReq;

    //     vkGetImageMemoryRequirements(device, image, &memReq);

    //     VkMemoryAllocateInfo memAI{};
    //     memAI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    //     memAI.memoryTypeIndex = chooseMemoryType(physicalDevice, memReq.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    //     memAI.allocationSize = memReq.size;

    //     vkAllocateMemory(device, &memAI, nullptr, &imageDeviceMemory);
    // }

    VkDescriptorPoolSize descPoolSizes{};
    descPoolSizes.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descPoolSizes.descriptorCount = 3;

    VkDescriptorPool descPool;
    VkDescriptorPoolCreateInfo descPoolCI{};
    descPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descPoolCI.poolSizeCount = 1;
    descPoolCI.pPoolSizes = &descPoolSizes;
    descPoolCI.maxSets = 1;

    vkRes = vkCreateDescriptorPool(device, &descPoolCI, nullptr, &descPool);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateDescriptorPool" << std::endl;
    }

    std::array<VkDescriptorSetLayoutBinding, 3> descSetLayoutBinding{};

    for(int i = 0; i < 3; i++)
    {
        descSetLayoutBinding[i].binding = i;
        descSetLayoutBinding[i].descriptorCount = 1;
        descSetLayoutBinding[i].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        descSetLayoutBinding[i].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    }    

    VkDescriptorSetLayout descSetLayout;
    VkDescriptorSetLayoutCreateInfo descSetLayoutCI{};
    descSetLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descSetLayoutCI.bindingCount = descSetLayoutBinding.size();
    descSetLayoutCI.pBindings = descSetLayoutBinding.data();

    vkRes = vkCreateDescriptorSetLayout(device, &descSetLayoutCI, nullptr, &descSetLayout);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreateDescriptorSetLayout" << std::endl;
    }

    VkDescriptorSet descSet;
    VkDescriptorSetAllocateInfo descSetAI{};
    descSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descSetAI.descriptorPool = descPool;
    descSetAI.descriptorSetCount = 1;
    descSetAI.pSetLayouts = &descSetLayout;

    vkAllocateDescriptorSets(device, &descSetAI, &descSet);

    // pipeline
    VkPushConstantRange pushConstantRange{};
    pushConstantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    // pushConstantRange.offset = 0;
    // pushConstantRange.size = 0;

    VkPipelineLayout pipelineLayout;

    VkPipelineLayoutCreateInfo layoutCI{};
    layoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    layoutCI.setLayoutCount = 1;
    layoutCI.pSetLayouts = &descSetLayout;

    VkSpecializationMapEntry specMapEntry{};
    specMapEntry.constantID = 0;
    specMapEntry.offset = 0;
    specMapEntry.size = sizeof(int);

    int max_iteration = 1000;
    VkSpecializationInfo specInfo{};
    specInfo.dataSize = sizeof(int);
    specInfo.pData = &max_iteration;
    specInfo.pMapEntries = &specMapEntry;
    specInfo.mapEntryCount = 1;

    vkRes = vkCreatePipelineLayout(device, &layoutCI, nullptr, &pipelineLayout);
    if(vkRes != VK_SUCCESS)
    {
        std::cout<<"Failed: vkCreatePipelineLayout" << std::endl;
    }

    VkPipeline computePipeline = createComputePipeline(device, shaderModule, pipelineLayout, specInfo);

    std::array<VkDescriptorImageInfo, 1> descImageInfo{};
    std::array<VkWriteDescriptorSet,1> writeDescSet{};

    descImageInfo[0].imageLayout = VK_IMAGE_LAYOUT_GENERAL;
    descImageInfo[0].imageView = imageView;

    writeDescSet[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescSet[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    writeDescSet[0].descriptorCount = 1;
    writeDescSet[0].dstSet = descSet;
    writeDescSet[0].dstBinding = 0;
    writeDescSet[0].pImageInfo = descImageInfo.data();
    
    vkUpdateDescriptorSets(device, writeDescSet.size(), writeDescSet.data(), 0, nullptr);

    VkCommandBufferBeginInfo cmdBufferBeginInfo{};
    cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    VkSubmitInfo submitInfo{};

    {
        vkBeginCommandBuffer(cmdBuffer, &cmdBufferBeginInfo);

        VkImageMemoryBarrier barrier {};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.srcQueueFamilyIndex = 0;
        barrier.dstQueueFamilyIndex = 0;
        barrier.image = swapchainImages[swapchainImageIndex];
        barrier.subresourceRange = imgSubRR;

        {	
            barrier.srcAccessMask = VK_ACCESS_NONE;
            barrier.dstAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
            barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            barrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;	
            vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
            vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline);
            vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout,0, 1, &descSet, 0, nullptr );
            vkCmdDispatch(cmdBuffer, WIDTH/32, HEIGHT/32, 1);

            barrier.srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
            barrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;		

            // check VK_PIPELINE_STAGE_TRANSFER_BIT
            vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0,
                                    0, nullptr, 0, nullptr, 1, &barrier);

            vkEndCommandBuffer(cmdBuffer);

            VkSubmitInfo submitInfo{};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &cmdBuffer;

            vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

            VkPresentInfoKHR presentInfo{};
            presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
            presentInfo.swapchainCount = 1;
            presentInfo.pSwapchains = &swapchain;
            presentInfo.pImageIndices = &swapchainImageIndex;
            presentInfo.pResults = &vkRes;

            vkRes = vkQueuePresentKHR(queue, &presentInfo);
            if (vkRes != VK_SUCCESS)
            {
                printf("Failed: vkCreateBuffer");
                return ;
            }

            while(vkQueueWaitIdle(queue) != VK_SUCCESS);

            vkResetCommandPool(device, cmdPool, 0);
        }
    }

    while(ProccessEvent() == false);
}
