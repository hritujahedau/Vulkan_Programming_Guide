#include "vulkan.h"

using namespace std;

uint32_t ChooseHeapFromFlag(VkPhysicalDevice physicalDevice, const VkMemoryRequirements& memReq, VkMemoryPropertyFlagBits flag)
{
	VkPhysicalDeviceMemoryProperties deviceMemProp{};
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &deviceMemProp);

	uint32_t selectedType = ~0u;
	uint32_t memoryType;

	for (memoryType = 0; memoryType < 32; memoryType++)
	{
		if (memReq.memoryTypeBits & (1 << memoryType))
		{
			const VkMemoryType& type = deviceMemProp.memoryTypes[memoryType];
			if ((type.propertyFlags & flag) == flag)
			{
				selectedType = memoryType;
				break;
			}
		}
	}
	return selectedType;
}

void main()
{
	VkResult vkRes;

	VkInstance instance;
	VkInstanceCreateInfo instanceCI{};

	VkApplicationInfo applicationInfo{};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = "Hrituja's Vulkan code";
	applicationInfo.applicationVersion = 1;
	applicationInfo.pEngineName = "Unreal";
	applicationInfo.apiVersion = VK_VERSION_1_0;

	char* layerName = "VK_LAYER_KHRONOS_validation";
	std::vector<const char*> extensionList {
		VK_KHR_SURFACE_EXTENSION_NAME,
		VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
	};
	instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCI.pApplicationInfo = &applicationInfo;
	instanceCI.enabledLayerCount = 1;
	instanceCI.ppEnabledLayerNames = &layerName;
	instanceCI.enabledExtensionCount = extensionList.size();	
	instanceCI.ppEnabledExtensionNames = extensionList.data();

	vkRes = vkCreateInstance(&instanceCI, nullptr, &instance);
	if (vkRes != VK_SUCCESS)
	{
		cout << "Failed: vkCreateInstance";
		return;
	}

	vector<VkPhysicalDevice> physicalDeviceVec;
	VkPhysicalDevice physicalDevice;
	uint32_t count;
	vkEnumeratePhysicalDevices(instance, &count, nullptr);

	physicalDeviceVec.resize(count);

	vkEnumeratePhysicalDevices(instance, &count, physicalDeviceVec.data());

	physicalDevice = physicalDeviceVec[0];

	uint32_t queueFamilyPropertyCount;
	uint32_t queueFamilyIndex = ~0u; // in VkQueueFamilyProperties's memebr index
	vector<VkQueueFamilyProperties> queueFamilyPropertiesVec;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);
	queueFamilyPropertiesVec.resize(queueFamilyPropertyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, queueFamilyPropertiesVec.data());

	for (int i = 0; i < queueFamilyPropertyCount; i++) {
		bool presentationSupport = vkGetPhysicalDeviceWin32PresentationSupportKHR(physicalDevice, i);
		if ((queueFamilyPropertiesVec[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) && presentationSupport) {
			queueFamilyIndex = i;
			break;
		}
	}

	HWND hwnd;
	HINSTANCE hInstance;
	createWindow(hwnd, hInstance);

	VkWin32SurfaceCreateInfoKHR surfaceCreateInfo{};
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.flags =  0;
	surfaceCreateInfo.hwnd = hwnd;
	surfaceCreateInfo.hinstance = hInstance;

	VkSurfaceKHR surface;
	vkRes = vkCreateWin32SurfaceKHR(instance, &surfaceCreateInfo, nullptr, &surface);
	if (vkRes != VK_SUCCESS)
	{
		cout << "Failed: vkCreateWin32SurfaceKHR";
		return;
	}

	printf("\n vkCreateWin32SurfaceKHR");

	VkDevice device;

	VkDeviceQueueCreateInfo deviceQCI{};
	deviceQCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQCI.queueCount = 1;
	deviceQCI.queueFamilyIndex = queueFamilyIndex ; 
	float queuePriorities = 1.0;
	deviceQCI.pQueuePriorities = &queuePriorities; 	

	std::vector<const char*> deviceExtensions {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};
	VkPhysicalDeviceFeatures requiredfeatures{};
	VkDeviceCreateInfo deviceCI{};
	deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCI.pQueueCreateInfos = &deviceQCI;
	deviceCI.queueCreateInfoCount = 1 ;
	deviceCI.pEnabledFeatures = &requiredfeatures;
	deviceCI.enabledExtensionCount = deviceExtensions.size();
	deviceCI.ppEnabledExtensionNames = deviceExtensions.data();
	
	uint32_t data[8] = {1,2,3,4,5,6,7,8};

	vkCreateDevice(physicalDevice, &deviceCI, nullptr, &device);

	printf("\n createDevice");

	VkSurfaceCapabilitiesKHR surfaceCapabilities{};

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);

	printf("\n vkGetPhysicalDeviceSurfaceCapabilitiesKHR");

	VkSwapchainKHR swapChain;
	VkSwapchainCreateInfoKHR swapChainCreateInfo{};

	swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapChainCreateInfo.surface = surface;
	swapChainCreateInfo.minImageCount = min((surfaceCapabilities.minImageCount + 1), surfaceCapabilities.maxImageCount);
	
	uint32_t surfaceFormatCount;
	vector<VkSurfaceFormatKHR> surfaceFormat;
	vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, nullptr);

	surfaceFormat.resize(surfaceFormatCount);

	vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, surfaceFormat.data());

	VkBool32 bQueueFamilyIndexSurfaceSupport;
	vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, &bQueueFamilyIndexSurfaceSupport);
	// selected queue family index has support
	// if(bQueueFamilyIndexSurfaceSupport)
	// {
	// 	printf("\nbQueueFamilyIndexSurfaceSupport");
	// }
	// else
	// {
	// 	printf("\nbQueueFamilyIndexSurfaceSupport not");
	// }

	swapChainCreateInfo.imageFormat = surfaceFormat[0].format;
	swapChainCreateInfo.imageColorSpace = surfaceFormat[0].colorSpace; //at present state only defined color space is VK_COLORSPACE_SRGB_NONLINEAR_KHR;
	swapChainCreateInfo.imageExtent = surfaceCapabilities.minImageExtent;
	swapChainCreateInfo.imageArrayLayers = 1;
	swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapChainCreateInfo.queueFamilyIndexCount = 1;
	swapChainCreateInfo.pQueueFamilyIndices = &queueFamilyIndex;
	swapChainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapChainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
	swapChainCreateInfo.clipped = VK_TRUE;
	swapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

	vkRes = vkCreateSwapchainKHR(device, &swapChainCreateInfo, nullptr, &swapChain);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkCreateSwapchainKHR()");
		return;
	}

	printf("\n vkCreateSwapchainKHR");

	uint32_t swapChainImageCount = 0;
	vector<VkImage> swapChainImages;

	vkRes = vkGetSwapchainImagesKHR(device, swapChain, &swapChainImageCount, nullptr);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkGetSwapchainImagesKHR()");
		return;
	}

	swapChainImages.resize(swapChainImageCount);
	vkRes = vkGetSwapchainImagesKHR(device, swapChain, &swapChainImageCount, swapChainImages.data());
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkGetSwapchainImagesKHR()");
		return;
	}

	uint32_t swapChainImageIndex;
	VkSemaphore semaphor;
	VkSemaphoreCreateInfo semaphoreCreateInfo{};
	semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	vkRes =  vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &semaphor);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkCreateSemaphore()");
		return;
	}

	vkRes = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, semaphor, VK_NULL_HANDLE, &swapChainImageIndex);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkAcquireNextImageKHR()");
		return;
	}

	// command 
	VkCommandPool commandPool;
	VkCommandPoolCreateInfo commandPoolCreateInfo{};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = queueFamilyIndex;
	commandPoolCreateInfo.flags = 0;

	vkRes = vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkCreateCommandPool()");
		return;
	}

	VkCommandBuffer commandBuffer;
	VkCommandBufferAllocateInfo cmdBufferAI{};
	cmdBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBufferAI.commandPool = commandPool;
	cmdBufferAI.commandBufferCount = 1;
	cmdBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

	vkRes = vkAllocateCommandBuffers(device, &cmdBufferAI, &commandBuffer);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkAllocateCommandBuffers()");
		return;
	}

	// create a queue
	VkQueue queue;
	vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue); 
	if (&queue == VK_NULL_HANDLE)
	{
		printf("Failed: to get a queue");
		return;
	}
	
	// recording the command buffers
	VkCommandBufferBeginInfo cmdBufferBegineInfo{};
	cmdBufferBegineInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	
	vkRes = vkBeginCommandBuffer(commandBuffer, &cmdBufferBegineInfo);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkBeginCommandBuffer()");
		return;
	}

	VkImageSubresourceRange subresourceRange{};
	subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	subresourceRange.baseMipLevel = 0;
	subresourceRange.levelCount = 1;
	subresourceRange.baseArrayLayer = 0;
	subresourceRange.layerCount = 1;

	VkImageMemoryBarrier barrier {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.srcQueueFamilyIndex = 0;
	barrier.dstQueueFamilyIndex = 0;
	barrier.image = swapChainImages[swapChainImageIndex];
	barrier.subresourceRange = subresourceRange;

	{	
		barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		barrier.dstAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
		barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;	

		vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
								0, nullptr, 0, nullptr, 1, &barrier);
		VkClearColorValue clearColorVal = {1.0f, 0.0f, 0.0f, 1.0f};
		vkCmdClearColorImage(commandBuffer, swapChainImages[swapChainImageIndex], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clearColorVal, 1, &subresourceRange);
	
	}
	{	
		barrier.srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;		

		// check VK_PIPELINE_STAGE_TRANSFER_BIT
		vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0,
								0, nullptr, 0, nullptr, 1, &barrier);
	}
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.pCommandBuffers = &commandBuffer;
	submitInfo.commandBufferCount = 1;

	vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

	VkPresentInfoKHR presentInfo{};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &swapChain;
	presentInfo.pImageIndices = &swapChainImageIndex;
	presentInfo.pResults = &vkRes;

	vkRes = vkQueuePresentKHR(queue, &presentInfo);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBuffer");
		return ;
	}

	vkRes = vkQueueWaitIdle(queue);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBuffer");
		return ;
	}

	while(ProccessEvent() == false);

	vkDestroySwapchainKHR(device, swapChain, nullptr);

}

