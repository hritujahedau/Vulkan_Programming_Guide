#include <stdio.h>
#include <iostream>
#include <vector>
#include <vulkan\vulkan.h>

using namespace std;

int main()
{
	VkResult vkRes;

	VkInstanceCreateInfo pCreateInfo{};
	VkApplicationInfo appInfo{};
	VkInstance instance{};
	
	VkPhysicalDevice physicalDevice{};
	uint32_t physicalDeviceCount = 1;

	VkDevice device{};
	VkDeviceCreateInfo deviceInfo{};
	
	
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pNext = nullptr;
	appInfo.pApplicationName = "Hrituja's vulkan program";
	appInfo.applicationVersion = 1;
	appInfo.apiVersion = VK_MAKE_VERSION(1.0, 0.0, 0.0);

	const char* layerNames = "VK_LAYER_KHRONOS_validation";

	pCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	pCreateInfo.pNext = nullptr;
	pCreateInfo.pApplicationInfo = &appInfo;
	pCreateInfo.enabledLayerCount = 1;
	pCreateInfo.ppEnabledLayerNames = &layerNames;
	pCreateInfo.enabledExtensionCount = 0;

	vkRes = vkCreateInstance(&pCreateInfo, nullptr, &instance);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateInfo");
		return 0;
	}

	// get physical device
	vkRes = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, &physicalDevice);
	// returns VK_INCOMPLETE because I have only for 1 device
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: VkEnumeratePhysicalDevice %d\n", vkRes);
		//return 0;
	}

	// create logical device
	VkDeviceQueueCreateInfo deviceQueueCreateInfo{};
	float f = 1.0;
	deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueCreateInfo.pNext = nullptr;
	deviceQueueCreateInfo.flags = 0;
	deviceQueueCreateInfo.queueCount = 1;
	deviceQueueCreateInfo.queueFamilyIndex = 0;
	deviceQueueCreateInfo.pQueuePriorities = &f;

	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceInfo.pNext = nullptr;
	deviceInfo.queueCreateInfoCount = 1;
	deviceInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
	deviceInfo.flags = 0;
	deviceInfo.enabledLayerCount = 0;
	deviceInfo.ppEnabledLayerNames = nullptr;
	deviceInfo.enabledExtensionCount = 0;
	deviceInfo.ppEnabledExtensionNames = nullptr;
	deviceInfo.pEnabledFeatures = nullptr;

	vkRes = vkCreateDevice(physicalDevice, &deviceInfo, nullptr, &device);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: VkCreateDevice");
		return 0;
	}
	
	uint32_t queueFamilyPropertyCount = 0;
	std::vector<VkQueueFamilyProperties> queueFamilyProperties;
	
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);
	
	printf("\nQueueFamilyPropertyCount = %d", queueFamilyPropertyCount);

	queueFamilyProperties.resize(queueFamilyPropertyCount);

	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, queueFamilyProperties.data());
	
	char* VkQueueFlagBits[] = {
		"VK_QUEUE_GRAPHICS_BIT" ,
		"VK_QUEUE_COMPUTE_BIT" ,
		"VK_QUEUE_TRANSFER_BIT" ,
		"VK_QUEUE_SPARSE_BINDING_BIT" ,
		// Provided by VK_VERSION_1_1
		"VK_QUEUE_PROTECTED_BIT" ,
		// Provided by VK_KHR_video_decode_queue
		"VK_QUEUE_VIDEO_DECODE_BIT_KHR" ,
		// Provided by VK_KHR_video_encode_queue
		"VK_QUEUE_VIDEO_ENCODE_BIT_KHR" ,
		// Provided by VK_NV_optical_flow
		"VK_QUEUE_OPTICAL_FLOW_BIT_NV" ,
	};

	const int VkQueueFlagCount = 8;
	printf("\n\nQueue family properties\n");

	for (int i = 0; i < queueFamilyPropertyCount; i++)
	{
		printf("\n\nqueueCount = %d", queueFamilyProperties[i].queueCount);
		printf("\ntimestampValidBits = %d", queueFamilyProperties[i].timestampValidBits);
		printf("\nwidth = %d, height = %d, depth = %d", queueFamilyProperties[i].minImageTransferGranularity.width,
			queueFamilyProperties[i].minImageTransferGranularity.height,
			queueFamilyProperties[i].minImageTransferGranularity.depth);
		cout << endl;
		for (int j = 0; j < VkQueueFlagCount; j++)
		{
			if ((1 << j) & queueFamilyProperties[i].queueFlags)
				printf("%s | ", VkQueueFlagBits[j]);
		}

		cout << endl;
	}

	printf("\n\n");

	VkQueue vkQueue;
	vkGetDeviceQueue(device, 0, 0, &vkQueue);
	if (&vkQueue == VK_NULL_HANDLE)
	{
		printf("Failed: to get a queue");
		return -1;
	}

	VkCommandPoolCreateInfo commandPoolCreateInfo =
	{
		VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		nullptr,
		// flag: VK_COMMAND_POOL_CREATE_TRANSIENT_BIT or VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
		0,
		0 // queueFamilyIndex
	};

	VkCommandPool commandPool;

	vkRes = vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateCommandPool");
		return 0;
	}

	VkCommandBufferAllocateInfo cmdBuffferAllocateInfo = 
	{
		VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		nullptr,
		commandPool,
		VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		1
	};

	VkCommandBuffer commandBuffer;

	vkRes = vkAllocateCommandBuffers(device, &cmdBuffferAllocateInfo, &commandBuffer);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkAllocateCommandBuffers");
		return 0;
	}

	// recording the command buffers
	VkCommandBufferBeginInfo cmdBufferBegin =
	{
		VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		nullptr,
		0, // flag
		nullptr, // const VkCommandBufferInheritanceInfo
		// 1. VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
		// 2. VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT
		// 3. VK_COMMAND_BUFFER_SIMULTANEAOUS_USE_BIT
	};

	vkRes = vkBeginCommandBuffer(commandBuffer, &cmdBufferBegin);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkBegineCommandBuffer");
		return 0;
	}

	// create 2 buffers to copy 
	VkBufferCreateInfo bufferInfo =
	{
		VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		nullptr,
		0,
		1024,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_SHARING_MODE_EXCLUSIVE,
		0,
		nullptr
	};

	VkBuffer buffer;

	vkRes = vkCreateBuffer(device, &bufferInfo, nullptr, &buffer);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBuffer");
		return 0;
	}

	vkCmdCopyBuffer(commandBuffer, buffer, buffer, 1, &bufferCopy);
	
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo{};

	submitInfo.sType = VK_STRCUTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;


	vkQueueSubmit(vkQueue, 1, &submitInfo, VK_NULL_HANDLE);

	// reset command buffer
	vkResetCommandBuffer(commandBuffer, VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCE_BIT);


	//reset command pull to reset all command buffers in one shot
	vkResetCommandPool(device, commandPool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCE_BIT);

	vkRes = vkQueueWaitIdle(vkQueue);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBuffer");
		return 0;
	}

	// or
	vkRes = vkDeviceWaitIdle(device);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBuffer");
		return 0;
	}

	// free or destroy the resource

	vkDestroyBuffer(device, buffer, nullptr);

	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
	
	vkDestroyCommandPool(device, commandPool, nullptr);

	vkDestroyDevice(device, nullptr);

	vkDestroyInstance(instance, nullptr);

	return 0;
}
