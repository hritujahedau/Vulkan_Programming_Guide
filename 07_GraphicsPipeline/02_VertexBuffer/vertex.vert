#version 450

layout(location = 0) in vec3 vertices;

// vec3 vertices[3] = 
// {
//     {0.0, -1.0, 0.0},
//     {-1.0, 1.0, 0.0},
//     {1.0, 1.0, 0.0}
// };

void main(void)
{
    gl_Position = vec4(vertices, 1.0);
}
