#version 450

layout(location = 0) in vec3 vertices;
layout(location = 1) in vec3 colors;

layout(location = 0) out vec3 color;

layout (constant_id = 0) const int instanceCount = 4;

layout(set = 0, binding = 0) uniform ubo_t
{
    mat4 mvp;
} ubo[instanceCount];

void main(void)
{
    gl_Position = ubo[gl_InstanceIndex].mvp * vec4(vertices, 1.0);
    color = colors;
}
