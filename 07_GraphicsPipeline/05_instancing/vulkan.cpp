#include "helper.h"

const uint32_t instanceCount = 4;

struct uniformBuffers_t
{
    glm::mat4 mvp;
};

std::vector<struct uniformBuffers_t> bufferMvp(instanceCount);

void main()
{
    VkInstance instance = createInstant();
    VkPhysicalDevice physicalDevice = enumerateAndGetPhysicalDevice(instance);
    uint32_t queueFamilyIndex = getQueueFamilyIndex(physicalDevice, VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT );
    uint32_t queueIndex = 0;
    VkDevice device = createDevice(physicalDevice, queueFamilyIndex);

    VkQueue queue ;
    vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, &queue);

    VkSurfaceKHR  surface = createSurface(instance);
    VkSwapchainKHR swapchain = createSwapchain(physicalDevice, device, surface, queueFamilyIndex);

    std::vector<VkImage> swapchainImages = getSwapChainImages(device, swapchain);
    std::vector<VkImageView> swapchainImageViews(swapchainImages.size());

    VkImageViewCreateInfo imgViewCI{};
    VkImageSubresourceRange imgSubRR{};

    imgSubRR.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imgSubRR.baseArrayLayer = 0;
    imgSubRR.baseMipLevel = 0;
    imgSubRR.layerCount = 1;
    imgSubRR.levelCount = 1;

    imgViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imgViewCI.format = VK_FORMAT_B8G8R8A8_UNORM; // save formatch while creating swap chain and set it here
    imgViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imgViewCI.subresourceRange = imgSubRR;

    for(int i = 0; i < swapchainImages.size(); i++)
    {
        imgViewCI.image = swapchainImages[i];
        vkCreateImageView(device, &imgViewCI, nullptr, &swapchainImageViews[i]);
    }

    VkCommandPool cmdPool = createCommandPool(device, queueFamilyIndex);
    VkCommandBuffer cmdBuffer = createCommandBuffer(device, cmdPool);

    // create vertex buffer and copy vertices 
    std::vector<glm::vec3> vertexBufferData = 
    {
        glm::vec3(0.0f, -1.0f, 0.0f),
        glm::vec3(-1.0f, 1.0f, 0.0f),
        glm::vec3(1.0f, 1.0f, 0.0f)
    };

    std::vector<glm::vec3> colorBufferData = 
    {
        glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f)
    };

    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    uint32_t *pMem;

    createBuffer(physicalDevice, device, queueFamilyIndex, vertexBufferData.size() * sizeof(glm::vec3), 
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                vertexBuffer, vertexBufferMemory);

    vkMapMemory(device, vertexBufferMemory, 0, VK_WHOLE_SIZE, 0, (void**)&pMem);
    memcpy(pMem, vertexBufferData.data(), vertexBufferData.size() * sizeof(glm::vec3));
    vkUnmapMemory(device, vertexBufferMemory);
                
    VkBuffer colorBuffer;
    VkDeviceMemory colorBufferMemory;

    createBuffer(physicalDevice, device, queueFamilyIndex, colorBufferData.size() * sizeof(glm::vec3), 
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                colorBuffer, colorBufferMemory);
    
    vkMapMemory(device, colorBufferMemory, 0, VK_WHOLE_SIZE, 0, (void**)&pMem);
    memcpy(pMem, colorBufferData.data(), colorBufferData.size() * sizeof(glm::vec3));
    vkUnmapMemory(device, colorBufferMemory);

    const std::vector<uint16_t> vertexIndexBufferData = {0, 1, 2};

    VkBuffer vertexIndexBuffer;
    VkDeviceMemory vertexIndexBufferMemory;
    
    createBuffer(physicalDevice, device, queueFamilyIndex, sizeof(uint16_t) * vertexIndexBufferData.size(), 
                VK_BUFFER_USAGE_INDEX_BUFFER_BIT, 
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                vertexIndexBuffer, vertexIndexBufferMemory);

    uint32_t *pVertIndexMemMap;
    vkMapMemory(device, vertexIndexBufferMemory, 0, VK_WHOLE_SIZE, 0, (void**)&pVertIndexMemMap);
    memcpy(pVertIndexMemMap, vertexIndexBufferData.data(), sizeof(uint16_t) * vertexIndexBufferData.size());
    vkUnmapMemory(device, vertexIndexBufferMemory);

    VkBuffer mvpUniformBuffer;
    VkDeviceMemory mvpUniformBufferMemory;

    createBuffer(physicalDevice, device, queueFamilyIndex, sizeof(uniformBuffers_t) * instanceCount, 
                VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, 
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                mvpUniformBuffer, mvpUniformBufferMemory);

    VkAttachmentDescription colorAttachmentDesc{};
    colorAttachmentDesc.format = VK_FORMAT_B8G8R8A8_UNORM;
    colorAttachmentDesc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachmentDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachmentDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachmentDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachmentDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachmentDesc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    colorAttachmentDesc.samples = VK_SAMPLE_COUNT_1_BIT;

    VkAttachmentReference colorAttachment{};
    colorAttachment.attachment = 0;
    colorAttachment.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachment;
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

    std::array<VkSubpassDependency, 1> dependencies{};

    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[0].srcAccessMask = VK_ACCESS_NONE;
    dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    // dependencies[1].srcSubpass = 0;
    // dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    // dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    // dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    // dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    // dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;    

    VkRenderPassCreateInfo renderPassCI{};
    renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassCI.subpassCount = 1;
    renderPassCI.pSubpasses = &subpass;
    renderPassCI.attachmentCount = 1;
    renderPassCI.pAttachments = &colorAttachmentDesc;
    renderPassCI.dependencyCount = dependencies.size();
    renderPassCI.pDependencies = dependencies.data();

    VkRenderPass renderPass;
    vkCreateRenderPass(device, &renderPassCI, nullptr, &renderPass);

    std::vector<VkFramebuffer> framebuffer(swapchainImageViews.size());
    VkFramebufferCreateInfo fboCI{};
    fboCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fboCI.attachmentCount = 1;
    fboCI.width = WIDTH;
    fboCI.height = HEIGHT;
    fboCI.layers = 1;
    fboCI.renderPass = renderPass;

    for(int i = 0; i < swapchainImageViews.size(); i++) 
    {
        fboCI.pAttachments = &swapchainImageViews[i];
        vkCreateFramebuffer(device, &fboCI, nullptr, &framebuffer[i]);
    }

    VkDescriptorSetLayoutBinding descSetLayoutBind;
    descSetLayoutBind.binding = 0;
    descSetLayoutBind.descriptorCount = 1;
    descSetLayoutBind.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descSetLayoutBind.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayout descSetLayout{};
    VkDescriptorSetLayoutCreateInfo descSetLayoutCI{};
    descSetLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descSetLayoutCI.bindingCount = 1;
    descSetLayoutCI.pBindings = &descSetLayoutBind;

    vkCreateDescriptorSetLayout(device, &descSetLayoutCI, nullptr, &descSetLayout);

    VkPipelineLayout pipelineLayout;
    VkPipelineLayoutCreateInfo pipelineLayoutCI{};
    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCI.pSetLayouts = &descSetLayout;
    pipelineLayoutCI.setLayoutCount = 1;
    
    VkResult vkRes = vkCreatePipelineLayout(device, &pipelineLayoutCI, nullptr, &pipelineLayout);

    VkDescriptorPoolSize poolSize{};
    poolSize.descriptorCount = 1;
    poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

    VkDescriptorPool descPool;
    VkDescriptorPoolCreateInfo descPoolCI{};
    descPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descPoolCI.poolSizeCount = 1;
    descPoolCI.pPoolSizes = &poolSize;
    descPoolCI.maxSets = 1;

    vkCreateDescriptorPool(device, &descPoolCI, nullptr, &descPool);

    VkDescriptorSet descSet;
    VkDescriptorSetAllocateInfo descSetAI{};
    descSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descSetAI.pSetLayouts = &descSetLayout;
    descSetAI.descriptorSetCount = 1;
    descSetAI.descriptorPool = descPool;

    vkAllocateDescriptorSets(device, &descSetAI, &descSet);

    std::array<VkShaderModule, 2> shaderModule;
    shaderModule[0] = createShaderModule(device, "vertex.vert.spv");
    shaderModule[1] = createShaderModule(device, "fragment.frag.spv");

    VkSpecializationMapEntry specMapEntry{};
    specMapEntry.constantID = 0;
    specMapEntry.offset = 0;
    specMapEntry.size = sizeof(uint32_t);

    VkSpecializationInfo specInfo{};
    specInfo.pData = &instanceCount;
    specInfo.dataSize = sizeof(uint32_t);
    specInfo.pMapEntries = &specMapEntry;
    specInfo.mapEntryCount = 1;

    std::array<VkPipelineShaderStageCreateInfo , 2> pipelineShaderStage{};
    // vertex shader
    pipelineShaderStage[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineShaderStage[0].module = shaderModule[0];
    pipelineShaderStage[0].pName = "main";
    pipelineShaderStage[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    pipelineShaderStage[0].pSpecializationInfo = &specInfo;

    // pixel shadeer
    pipelineShaderStage[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineShaderStage[1].module = shaderModule[1];
    pipelineShaderStage[1].pName = "main";
    pipelineShaderStage[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;

    //VkPipelineVertexInputStateCreateInfo vertStageCI{};
    //vertStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo colorBlendState{};
    colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendState.attachmentCount = 1;
    colorBlendState.pAttachments = &colorBlendAttachment;

    VkPipelineDepthStencilStateCreateInfo depthStencilState{};
    depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    // and more 

    VkPipelineDynamicStateCreateInfo dynamicState{};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyState{};
    inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    VkPipelineMultisampleStateCreateInfo multiSampleState{};
    multiSampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multiSampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineRasterizationStateCreateInfo rasterizationState{};
    rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationState.cullMode = VK_CULL_MODE_NONE;
    rasterizationState.frontFace= VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizationState.lineWidth = 1.0;
    rasterizationState.polygonMode = VK_POLYGON_MODE_FILL; 

    VkPipelineTessellationStateCreateInfo tessellationState{};
    tessellationState.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;

    std::array<VkVertexInputBindingDescription, 2> vertInBindingDesc{};
    vertInBindingDesc[0].binding = 0;
    vertInBindingDesc[0].stride = sizeof(glm::vec3);
    vertInBindingDesc[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    vertInBindingDesc[1].binding = 1;
    vertInBindingDesc[1].stride = sizeof(glm::vec3);
    vertInBindingDesc[1].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    
    std::array<VkVertexInputAttributeDescription, 2> vertAttribBindingDesc{};
    vertAttribBindingDesc[0].binding = 0;
    vertAttribBindingDesc[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertAttribBindingDesc[0].offset = 0;
    vertAttribBindingDesc[0].location = 0;

    vertAttribBindingDesc[1].binding = 1; // vertInBindingDesc[1].binding = 1;
    vertAttribBindingDesc[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertAttribBindingDesc[1].offset = 0;
    vertAttribBindingDesc[1].location = 1;

    VkPipelineVertexInputStateCreateInfo vertexInputState{};
    vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputState.vertexBindingDescriptionCount = vertInBindingDesc.size();
    vertexInputState.pVertexBindingDescriptions = vertInBindingDesc.data();
    vertexInputState.vertexAttributeDescriptionCount = vertAttribBindingDesc.size();
    vertexInputState.pVertexAttributeDescriptions = vertAttribBindingDesc.data();

    VkViewport viewport{};
    viewport.width = WIDTH;
    viewport.height = HEIGHT;
    viewport.maxDepth = 1;
    viewport.minDepth = 0;
    viewport.x = 0;
    viewport.y = 0;
    
    VkRect2D scissor{};
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent.width = WIDTH;
    scissor.extent.height = HEIGHT;

    VkPipelineViewportStateCreateInfo viewportState{};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    VkGraphicsPipelineCreateInfo graphicsPipelineCI{};
    graphicsPipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphicsPipelineCI.flags = 0;
    graphicsPipelineCI.layout = pipelineLayout;
    graphicsPipelineCI.renderPass = renderPass;
    graphicsPipelineCI.subpass = 0;
    graphicsPipelineCI.basePipelineIndex = -1;
    graphicsPipelineCI.pColorBlendState = &colorBlendState;
    graphicsPipelineCI.pDepthStencilState = &depthStencilState;
    graphicsPipelineCI.pDynamicState = &dynamicState;
    graphicsPipelineCI.pInputAssemblyState = &inputAssemblyState;
    graphicsPipelineCI.pMultisampleState = &multiSampleState;
    graphicsPipelineCI.pRasterizationState = &rasterizationState;
    graphicsPipelineCI.pTessellationState = &tessellationState;
    graphicsPipelineCI.pVertexInputState = &vertexInputState;
    graphicsPipelineCI.pViewportState = &viewportState;
    graphicsPipelineCI.stageCount = pipelineShaderStage.size();
    graphicsPipelineCI.pStages = pipelineShaderStage.data();     

    VkPipeline pipeline;
    vkCreateGraphicsPipelines(device,  nullptr, 1, &graphicsPipelineCI, nullptr, &pipeline);

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkCommandBufferBeginInfo cmdBufferBI{};
    cmdBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    VkPresentInfoKHR present{};
    present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;   

    VkImageMemoryBarrier imgMemoryBarrier{};
    imgMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER; 

    glm::mat4 perspectiveMat = glm::mat4(1.0);
    glm::mat4 translateMat = glm::mat4(1.0);
    glm::mat4 size = glm::mat4(1.0);

    perspectiveMat = glm::perspective(glm::radians(45.0f), 1.0f, 0.1f, 100.0f);

    // 1
    translateMat = glm::translate(glm::mat4(1.0), glm::vec3(-1.0f, 1.0f, -3.0f));
    size = glm::scale(glm::mat4(1.0), glm::vec3(0.2f, 0.2f, 0.2f));
    bufferMvp[0].mvp = perspectiveMat * translateMat * size;

    // 2
    translateMat = glm::translate(glm::mat4(1.0), glm::vec3(1.0f, 1.0f, -3.0f));
    bufferMvp[1].mvp = perspectiveMat * translateMat * size;

    // 3
    translateMat = glm::translate(glm::mat4(1.0), glm::vec3(1.0f, -1.0f, -3.0f));
    bufferMvp[2].mvp = perspectiveMat * translateMat * size;

    // 4
    translateMat = glm::translate(glm::mat4(1.0), glm::vec3(-1.0f, -1.0f, -3.0f));
    bufferMvp[3].mvp = perspectiveMat * translateMat * size;

    uint32_t *pmvpUniformBufferMemMap;
    vkMapMemory(device, mvpUniformBufferMemory, 0, sizeof(uniformBuffers_t) * instanceCount, 0, (void**)&pmvpUniformBufferMemMap);
    memcpy(pmvpUniformBufferMemMap, bufferMvp.data(), sizeof(uniformBuffers_t) * instanceCount);
    vkUnmapMemory(device, mvpUniformBufferMemory);

    VkDescriptorBufferInfo descBufferInfo{};
    descBufferInfo.buffer = mvpUniformBuffer;
    descBufferInfo.offset = 0;
    descBufferInfo.range = sizeof(uniformBuffers_t) * instanceCount;

    VkWriteDescriptorSet writeDescSet{};
    writeDescSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescSet.descriptorCount = 1;
    writeDescSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writeDescSet.dstSet = descSet;
    writeDescSet.dstBinding = 0;
    writeDescSet.pBufferInfo = &descBufferInfo;

    vkUpdateDescriptorSets(device, 1, &writeDescSet, 0, nullptr);

    VkSemaphore semaphor;
    VkSemaphoreCreateInfo semaphoreCreateInfo{};
    semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    vkRes =  vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &semaphor);
    if (vkRes != VK_SUCCESS) 
    {
        printf("Failed: vkCreateSemaphore()");
        exit(ERROR);
    }

    // draw
    while(ProccessEvent() != true)
    {
        uint32_t imageIndex = aquireNextImage(device, swapchain, semaphor);
        
        VkClearValue clearColor;
        clearColor.color = {0.5f, 0.5f, 0.5f, 1.0f};

        vkBeginCommandBuffer(cmdBuffer,&cmdBufferBI); 
        VkRenderPassBeginInfo renderpassBI{};
        renderpassBI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderpassBI.framebuffer = framebuffer[imageIndex];
        renderpassBI.renderPass = renderPass;
        renderpassBI.renderArea.extent = {WIDTH, HEIGHT};
        renderpassBI.clearValueCount = 1;
        renderpassBI.pClearValues = &clearColor;

        vkCmdBeginRenderPass(cmdBuffer, &renderpassBI, VK_SUBPASS_CONTENTS_INLINE);

        VkDeviceSize offset = 0;        
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
        vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descSet, 0, 0);
        vkCmdBindVertexBuffers(cmdBuffer, 1, 1, &colorBuffer, &offset);
        vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &vertexBuffer, &offset);
        vkCmdBindIndexBuffer(cmdBuffer, vertexIndexBuffer, 0, VK_INDEX_TYPE_UINT16);
        vkCmdDrawIndexed(cmdBuffer, vertexIndexBufferData.size(), instanceCount, 0, 0, 0);

        vkCmdEndRenderPass(cmdBuffer);

        vkEndCommandBuffer(cmdBuffer); 

        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &cmdBuffer;
        
        vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(queue);

        present.swapchainCount = 1;
        present.pSwapchains = &swapchain;
        present.pImageIndices = &imageIndex;
        present.waitSemaphoreCount = 1;
        present.pWaitSemaphores = &semaphor;
        
        vkQueuePresentKHR(queue, &present);

        vkQueueWaitIdle(queue);

        vkResetCommandPool(device, cmdPool, 0);
    }

}