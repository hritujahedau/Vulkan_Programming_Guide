#version 450

vec3 vertices[3] = 
{
    {0.0, -1.0, 0.0},
    {-1.0, 1.0, 0.0},
    {1.0, 1.0, 0.0}
};

void main(void)
{
    gl_Position = vec4(vertices[gl_VertexIndex], 1.0);
}
