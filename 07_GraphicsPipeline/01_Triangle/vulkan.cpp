#include "helper.h"

struct push_constant_t
{
    float cx;
    float cy;
} push_constant {0.285, 0.0} ;

void main()
{
    VkInstance instance = createInstant();
    VkPhysicalDevice physicalDevice = enumerateAndGetPhysicalDevice(instance);
    uint32_t queueFamilyIndex = getQueueFamilyIndex(physicalDevice, VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT );
    uint32_t queueIndex = 0;
    VkDevice device = createDevice(physicalDevice, queueFamilyIndex);

    VkQueue queue ;
    vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, &queue);

    VkSurfaceKHR  surface = createSurface(instance);
    VkSwapchainKHR swapchain = createSwapchain(physicalDevice, device, surface, queueFamilyIndex);

    std::vector<VkImage> swapchainImages = getSwapChainImages(device, swapchain);
    std::vector<VkImageView> swapchainImageViews(swapchainImages.size());

    VkImageViewCreateInfo imgViewCI{};
    VkImageSubresourceRange imgSubRR{};

    imgSubRR.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imgSubRR.baseArrayLayer = 0;
    imgSubRR.baseMipLevel = 0;
    imgSubRR.layerCount = 1;
    imgSubRR.levelCount = 1;

    imgViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imgViewCI.format = VK_FORMAT_B8G8R8A8_UNORM;
    imgViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imgViewCI.subresourceRange = imgSubRR;

    for(int i = 0; i < swapchainImages.size(); i++)
    {
        imgViewCI.image = swapchainImages[i];
        vkCreateImageView(device, &imgViewCI, nullptr, &swapchainImageViews[i]);
    }

    VkCommandPool cmdPool = createCommandPool(device, queueFamilyIndex);
    VkCommandBuffer cmdBuffer = createCommandBuffer(device, cmdPool);

    VkAttachmentDescription colorAttachmentDesc{};
    colorAttachmentDesc.format = VK_FORMAT_B8G8R8A8_UNORM;
    colorAttachmentDesc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachmentDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachmentDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachmentDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachmentDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachmentDesc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    colorAttachmentDesc.samples = VK_SAMPLE_COUNT_1_BIT;

    VkAttachmentReference colorAttachment{};
    colorAttachment.attachment = 0;
    colorAttachment.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachment;
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

    std::array<VkSubpassDependency, 2> dependencies{};

    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[0].srcAccessMask = VK_ACCESS_NONE;
    dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    dependencies[1].srcSubpass = 0;
    dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;    

    VkRenderPassCreateInfo renderPassCI{};
    renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassCI.subpassCount = 1;
    renderPassCI.pSubpasses = &subpass;
    renderPassCI.attachmentCount = 1;
    renderPassCI.pAttachments = &colorAttachmentDesc;
    renderPassCI.dependencyCount = dependencies.size();
    renderPassCI.pDependencies = dependencies.data();

    VkRenderPass renderPass;
    vkCreateRenderPass(device, &renderPassCI, nullptr, &renderPass);

    std::vector<VkFramebuffer> framebuffer(swapchainImageViews.size());
    VkFramebufferCreateInfo fboCI{};
    fboCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fboCI.attachmentCount = 1;
    fboCI.width = WIDTH;
    fboCI.height = HEIGHT;
    fboCI.layers = 1;
    fboCI.renderPass = renderPass;

    for(int i = 0; i < swapchainImageViews.size(); i++) 
    {
        fboCI.pAttachments = &swapchainImageViews[i];
        vkCreateFramebuffer(device, &fboCI, nullptr, &framebuffer[i]);
    }

    VkPipelineLayout pipelineLayout;
    VkPipelineLayoutCreateInfo pipelineLayoutCI{};
    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    
    vkCreatePipelineLayout(device, &pipelineLayoutCI, nullptr, &pipelineLayout);

    std::array<VkShaderModule, 2> shaderModule;
    shaderModule[0] = createShaderModule(device, "vertex.vert.spv");
    shaderModule[1] = createShaderModule(device, "fragment.frag.spv");

    std::array<VkPipelineShaderStageCreateInfo , 2> pipelineShaderStage{};
    // vertex shader
    pipelineShaderStage[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineShaderStage[0].module = shaderModule[0];
    pipelineShaderStage[0].pName = "main";
    pipelineShaderStage[0].stage = VK_SHADER_STAGE_VERTEX_BIT;

    // pixel shadeer
    pipelineShaderStage[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineShaderStage[1].module = shaderModule[1];
    pipelineShaderStage[1].pName = "main";
    pipelineShaderStage[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;

    //VkPipelineVertexInputStateCreateInfo vertStageCI{};
    //vertStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo colorBlendState{};
    colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendState.attachmentCount = 1;
    colorBlendState.pAttachments = &colorBlendAttachment;

    VkPipelineDepthStencilStateCreateInfo depthStencilState{};
    depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    // and more 

    std::vector<VkDynamicState> dynamicState = {VK_DYNAMIC_STATE_VIEWPORT , VK_DYNAMIC_STATE_SCISSOR};
    VkPipelineDynamicStateCreateInfo dynamicStateCI{};
    dynamicStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicStateCI.dynamicStateCount = dynamicState.size();
    dynamicStateCI.pDynamicStates = dynamicState.data();

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyState{};
    inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    VkPipelineMultisampleStateCreateInfo multiSampleState{};
    multiSampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multiSampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineRasterizationStateCreateInfo rasterizationState{};
    rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationState.cullMode = VK_CULL_MODE_NONE;
    rasterizationState.frontFace= VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizationState.lineWidth = 1.0;
    rasterizationState.polygonMode = VK_POLYGON_MODE_FILL; 

    VkPipelineTessellationStateCreateInfo tessellationState{};
    tessellationState.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;

    VkPipelineVertexInputStateCreateInfo vertexInputState{};
    vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    VkViewport viewport{};
    viewport.width = WIDTH;
    viewport.height = HEIGHT;
    viewport.maxDepth = 1;
    viewport.minDepth = 0;
    viewport.x = 0;
    viewport.y = 0;
    
    VkRect2D scissor{};
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent.width = WIDTH;
    scissor.extent.height = HEIGHT;

    VkPipelineViewportStateCreateInfo viewportState{};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    VkGraphicsPipelineCreateInfo graphicsPipelineCI{};
    graphicsPipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphicsPipelineCI.flags = 0;
    graphicsPipelineCI.layout = pipelineLayout;
    graphicsPipelineCI.renderPass = renderPass;
    graphicsPipelineCI.subpass = 0;
    graphicsPipelineCI.basePipelineIndex = -1;
    graphicsPipelineCI.pColorBlendState = &colorBlendState;
    graphicsPipelineCI.pDepthStencilState = &depthStencilState;
    graphicsPipelineCI.pDynamicState = &dynamicStateCI;
    graphicsPipelineCI.pInputAssemblyState = &inputAssemblyState;
    graphicsPipelineCI.pMultisampleState = &multiSampleState;
    graphicsPipelineCI.pRasterizationState = &rasterizationState;
    graphicsPipelineCI.pTessellationState = &tessellationState;
    graphicsPipelineCI.pVertexInputState = &vertexInputState;
    graphicsPipelineCI.pViewportState = &viewportState;
    graphicsPipelineCI.stageCount = pipelineShaderStage.size();
    graphicsPipelineCI.pStages = pipelineShaderStage.data();
        

    VkPipeline pipeline;
    vkCreateGraphicsPipelines(device,  nullptr, 1, &graphicsPipelineCI, nullptr, &pipeline);

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkCommandBufferBeginInfo cmdBufferBI{};
    cmdBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    VkPresentInfoKHR present{};
    present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;   

    VkImageMemoryBarrier imgMemoryBarrier{};
    imgMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER; 

    while(ProccessEvent() != true)
    {
        uint32_t imageIndex = aquireNextImage(device, swapchain);    
        const VkImage& image = swapchainImages[imageIndex];
        
        VkClearValue clearColor;
        clearColor.color = {0.5f, 0.5f, 0.5f, 1.0f};

        vkBeginCommandBuffer(cmdBuffer,&cmdBufferBI); 
        VkRenderPassBeginInfo renderpassBI{};
        renderpassBI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderpassBI.framebuffer = framebuffer[imageIndex];
        renderpassBI.renderPass = renderPass;
        renderpassBI.renderArea.extent = {WIDTH, HEIGHT};
        renderpassBI.clearValueCount = 1;
        renderpassBI.pClearValues = &clearColor;

        vkCmdBeginRenderPass(cmdBuffer, &renderpassBI, VK_SUBPASS_CONTENTS_INLINE);

        VkViewport viewport{};
        viewport.width = WIDTH;
        viewport.height = HEIGHT;
        viewport.x = 0;
        viewport.y = 0;
        viewport.minDepth = 1;
        viewport.maxDepth = 1;
        vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);

        VkRect2D scissor{};
        scissor.extent = {WIDTH, HEIGHT};
        scissor.offset.x= 0;
        scissor.offset.y= 0;

        vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);

        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
        vkCmdDraw(cmdBuffer, 3, 1, 0, 0);

        vkCmdEndRenderPass(cmdBuffer);

        vkEndCommandBuffer(cmdBuffer); 

        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &cmdBuffer;
        
        vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(queue);

        present.swapchainCount = 1;
        present.pSwapchains = &swapchain;
        present.pImageIndices = &imageIndex;
        
        vkQueuePresentKHR(queue, &present);

        vkQueueWaitIdle(queue);

        vkResetCommandPool(device, cmdPool, 0);
    }

}