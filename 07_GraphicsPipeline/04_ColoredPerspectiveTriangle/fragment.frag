#version 450

layout(location = 0) out vec4 fragColor_out;

layout(location = 0) in vec3 color;

void main()
{
    fragColor_out = vec4(color, 1.0);
}
