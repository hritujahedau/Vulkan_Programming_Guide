#version 450

layout(location = 0) in vec3 vertices;
layout(location = 1) in vec3 colors;

layout(location = 0) out vec3 color;

layout(set = 0, binding = 0) uniform push_constant_t
{
    mat4 mvp;
} push_constant;

void main(void)
{
    gl_Position = push_constant.mvp * vec4(vertices, 1.0);
    color = colors;
}
