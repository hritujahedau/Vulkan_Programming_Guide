#include <stdio.h>
#include <vulkan\vulkan.h>

int main()
{
	VkResult vkRes;

	VkInstanceCreateInfo pCreateInfo{};
	VkApplicationInfo appInfo{};
	VkInstance instance{};
	
	VkPhysicalDevice physicalDevice{};
	uint32_t physicalDeviceCount = 1;

	VkDevice device{};
	VkDeviceCreateInfo deviceInfo{};
	
	
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pNext = nullptr;
	appInfo.pApplicationName = "Hrituja's vulkan program";
	appInfo.applicationVersion = 1;
	appInfo.apiVersion = VK_MAKE_VERSION(1.0, 0.0, 0.0);

	const char* layerNames = "VK_LAYER_KHRONOS_validation";

	pCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	pCreateInfo.pNext = nullptr;
	pCreateInfo.pApplicationInfo = &appInfo;
	pCreateInfo.enabledLayerCount = 1;
	pCreateInfo.ppEnabledLayerNames = &layerNames;
	pCreateInfo.enabledExtensionCount = 0;

	vkRes = vkCreateInstance(&pCreateInfo, nullptr, &instance);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateInfo");
		return 0;
	}

	// get physical device
	vkRes = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, &physicalDevice);
	// returns VK_INCOMPLETE because I have only for 1 device
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: VkEnumeratePhysicalDevice %d", vkRes);
		return 0;
	}

	// create logical device
	VkDeviceQueueCreateInfo deviceQueueCreateInfo{};
	float f = 1.0;
	deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueCreateInfo.pNext = nullptr;
	deviceQueueCreateInfo.flags = 0;
	deviceQueueCreateInfo.queueCount = 1;
	deviceQueueCreateInfo.queueFamilyIndex = 0;
	deviceQueueCreateInfo.pQueuePriorities = &f;

	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceInfo.pNext = nullptr;
	deviceInfo.queueCreateInfoCount = 1;
	deviceInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
	deviceInfo.flags = 0;
	deviceInfo.enabledLayerCount = 0;
	deviceInfo.ppEnabledLayerNames = nullptr;
	deviceInfo.enabledExtensionCount = 0;
	deviceInfo.ppEnabledExtensionNames = nullptr;
	deviceInfo.pEnabledFeatures = nullptr;

	vkRes = vkCreateDevice(physicalDevice, &deviceInfo, nullptr, &device);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: VkCreateDevice");
		return 0;
	}

	while(vkDeviceWaitIdle(device) != VK_SUCCESS);

	vkDestroyDevice(device, nullptr);

	vkDestroyInstance(instance, nullptr);

	return 0;
}
