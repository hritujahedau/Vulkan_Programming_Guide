#include <iostream>
#include <memory>
#include <vector>
#include <vulkan\vulkan.h>

using namespace std;

#define H_VK_SEPERATE_API_VERSION(version) \
        printf("%d.%d.%d.%d", VK_API_VERSION_VARIANT(version), VK_API_VERSION_MAJOR(version), \
        VK_API_VERSION_MINOR(version), VK_API_VERSION_PATCH(version));

#define PRINT_BOOLEAN_RESULT(result) ((result == VK_TRUE)? "Supported" : "Not Supported")
#define PRINT_VULKAN_RESULT(result) ((result == VK_SUCCESS)? printf("\nSuccess: Vulkan API executed successfully") : printf("\nFail: Vulkan API executed unsuccessfully"))

int main()
{
    VkResult result = VK_SUCCESS;
    VkApplicationInfo appInfo = {};
    VkInstanceCreateInfo instanceCreateInfo = {};
    VkInstance vkInstance{};
    uint32_t physicalDeviceCount = 0;
    VkPhysicalDevice* pPhysicalDevice{};
    VkPhysicalDeviceProperties pVkPhysicalDeviceProperties{};
    VkPhysicalDeviceFeatures supportedDeviceFeatures{};
    VkPhysicalDeviceFeatures requiredDeviceFeatures{};
    VkPhysicalDeviceMemoryProperties pVkPhysicalDeviceMemoryProperties{};
    VkDevice logicalDevice;

    char* propertyFlagBits[] = {
        "VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT" ,
        "VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT" ,
        "VK_MEMORY_PROPERTY_HOST_COHERENT_BIT" ,
        "VK_MEMORY_PROPERTY_HOST_CACHED_BIT" ,
        "VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT" ,
        "VK_MEMORY_PROPERTY_PROTECTED_BIT ",
        "VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD ",
        "VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD ",
        "VK_MEMORY_PROPERTY_RDMA_CAPABLE_BIT_NV" ,
    };

    const int propertyFlagCount = 9;

    char* VkMemoryHeapFlagBits[] = {
        "VK_MEMORY_HEAP_DEVICE_LOCAL_BIT",
        "VK_MEMORY_HEAP_MULTI_INSTANCE_BIT",
        "VK_MEMORY_HEAP_MULTI_INSTANCE_BIT_KHR",
    };

    const int memoryHeapFlagsCount = 3;

    uint32_t queueFamiltyPropertyCount = 0;
    VkQueueFamilyProperties* pVkQueueFamilyProperties = NULL;

    char* VkQueueFlagBits[] = {
        "VK_QUEUE_GRAPHICS_BIT" ,
        "VK_QUEUE_COMPUTE_BIT" ,
        "VK_QUEUE_TRANSFER_BIT" ,
        "VK_QUEUE_SPARSE_BINDING_BIT" ,
        // Provided by VK_VERSION_1_1
        "VK_QUEUE_PROTECTED_BIT" ,
        // Provided by VK_KHR_video_decode_queue
        "VK_QUEUE_VIDEO_DECODE_BIT_KHR" ,
        // Provided by VK_KHR_video_encode_queue
        "VK_QUEUE_VIDEO_ENCODE_BIT_KHR" ,
        // Provided by VK_NV_optical_flow
        "VK_QUEUE_OPTICAL_FLOW_BIT_NV" ,
    };

    const int VkQueueFlagCount = 8;

    uint32_t numLayers = 0;
    uint32_t numExtensions = 0;
    std::vector<VkLayerProperties> instanceLayerProperties;
    std::vector<VkExtensionProperties> instanceExtensionProperties;
    

    printf("\nStarting vulkan program\n");

    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Application";
    appInfo.applicationVersion = 1;
    appInfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);

    const char* layerList = "VK_LAYER_KHRONOS_validation";

    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pApplicationInfo = &appInfo;
    instanceCreateInfo.enabledLayerCount = 1;
    instanceCreateInfo.ppEnabledLayerNames = &layerList;

    result = vkCreateInstance(&instanceCreateInfo, nullptr, &vkInstance);
    if (result == VK_SUCCESS)
    {        
        result = vkEnumeratePhysicalDevices(vkInstance, &physicalDeviceCount, nullptr);
        printf("\nphysicalDeviceCount = %d", physicalDeviceCount);
        if (result == VK_SUCCESS)
        {
            // Size the device array appropriately and get the physical
            // device handles.
            pPhysicalDevice = (VkPhysicalDevice*)malloc(sizeof(VkPhysicalDevice) * physicalDeviceCount);
            result = vkEnumeratePhysicalDevices(vkInstance, &physicalDeviceCount, &pPhysicalDevice[0]);
            if (result == VK_SUCCESS)
            {
                printf("\nFailed: vkEnumeratePhysicalDevices with %d", result);
                //return 0;
            }
        }
    }
    else
    {
        printf("\nFailed: vkGetPhysicalDeviceProperties");
        //return 0;
    }
        
    for (int i = 0; i < physicalDeviceCount; i++)
    {
        vkGetPhysicalDeviceProperties(pPhysicalDevice[i], &pVkPhysicalDeviceProperties);
        printf("\n\ndeviceName = %s", pVkPhysicalDeviceProperties.deviceName);
        printf("\n");
        H_VK_SEPERATE_API_VERSION(pVkPhysicalDeviceProperties.apiVersion);
        printf("\ndriverVersion = %lu", pVkPhysicalDeviceProperties.driverVersion);
        printf("\nvendorID = %lu", pVkPhysicalDeviceProperties.vendorID);
        printf("\ndeviceID = %lu", pVkPhysicalDeviceProperties.deviceID);

        if (2 == pVkPhysicalDeviceProperties.deviceType)
        {
            printf("\nPhysical device type is discrete type");
        }
        
        printf("\npipelineCacheUUID = %d", pVkPhysicalDeviceProperties.pipelineCacheUUID[0]);
        printf("\nlimits = %u", pVkPhysicalDeviceProperties.limits.maxTexelBufferElements);
    }
    vkGetPhysicalDeviceFeatures(*pPhysicalDevice, &supportedDeviceFeatures);

    printf("\nPhysical Device Features:");
    printf("\nmultiDrawIndirect = %s", PRINT_BOOLEAN_RESULT(supportedDeviceFeatures.multiDrawIndirect));
    printf("\ndrawIndirectFirstInstance = %s", PRINT_BOOLEAN_RESULT(supportedDeviceFeatures.drawIndirectFirstInstance));
    printf("\nocclusionQueryPrecise = %s", PRINT_BOOLEAN_RESULT(supportedDeviceFeatures.occlusionQueryPrecise));
    printf("\nshaderTessellationAndGeometryPointSize = %s", PRINT_BOOLEAN_RESULT(supportedDeviceFeatures.shaderTessellationAndGeometryPointSize));
    printf("\nvariableMultisampleRate = %s", PRINT_BOOLEAN_RESULT(supportedDeviceFeatures.variableMultisampleRate));
   
    vkGetPhysicalDeviceMemoryProperties(*pPhysicalDevice, &pVkPhysicalDeviceMemoryProperties);
  
    printf("\n\nVkPhysicalDeviceMemoryProperties :");
    for (int i = 0; i < pVkPhysicalDeviceMemoryProperties.memoryTypeCount; i++)
    {
        // why 10: 10 is number of memory type, and loop 10 times for checking each bit.
        printf("\nMemory type %d\n", i);
        for (int j = 0; j < 10; j++) {
            if((1 << j) & pVkPhysicalDeviceMemoryProperties.memoryTypes[i].propertyFlags)
                printf("%s | ", propertyFlagBits[j]);
        }
    }

    printf("\b");
    
    printf("\n\nMemory heap:");
    for (int i = 0; i < 32; i++)
    {
        for (int j = 0; j < 10; j++) {
            //if ((1 << j) & pVkPhysicalDeviceMemoryProperties.memoryHeaps[i].flags)
                //printf("\nindex %d = %s", i, VkMemoryHeapFlagBits[j]);
        }
    }

    vkGetPhysicalDeviceQueueFamilyProperties(*pPhysicalDevice, &queueFamiltyPropertyCount, nullptr);
    printf("\nQueueFamiltyPropertyCount = %d", queueFamiltyPropertyCount);
    pVkQueueFamilyProperties = (VkQueueFamilyProperties*)malloc(sizeof(VkQueueFamilyProperties) * queueFamiltyPropertyCount);

    vkGetPhysicalDeviceQueueFamilyProperties(*pPhysicalDevice, &queueFamiltyPropertyCount, pVkQueueFamilyProperties);

    for (int i = 0; i < queueFamiltyPropertyCount; i++)
    {
        printf("\n\nqueueCount = %d", pVkQueueFamilyProperties[i].queueCount);
        printf("\ntimestampValidBits = %d", pVkQueueFamilyProperties[i].timestampValidBits);
        printf("\nqueueCount = %d", pVkQueueFamilyProperties[i].queueCount);
        printf("\nwidth = %d, height = %d, depth = %d", pVkQueueFamilyProperties[i].minImageTransferGranularity.width,
            pVkQueueFamilyProperties[i].minImageTransferGranularity.height,
            pVkQueueFamilyProperties[i].minImageTransferGranularity.depth);
        cout << endl;
        for (int j = 0; j < VkQueueFlagCount; j++)
        {
            if ((1 << j) & pVkQueueFamilyProperties[i].queueFlags)
                printf("%s | ", VkQueueFlagBits[j]);
        }
        
        cout << endl;
    }
        
    requiredDeviceFeatures.multiDrawIndirect = supportedDeviceFeatures.multiDrawIndirect;
    requiredDeviceFeatures.tessellationShader = VK_TRUE;
    requiredDeviceFeatures.geometryShader = VK_TRUE;    
   
    // passed dummy value for now, will change the value in coming chapter
    float queuePriority = 1.0;

    const VkDeviceQueueCreateInfo deviceQueueCreateInfo =
    {
        VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        nullptr,
        0, // flags
        0, // queueFamilyIndex
        1, // queueCount
        &queuePriority // pQueuePriorities
    };

    const VkDeviceCreateInfo deviceCreateInfo =
    {
        VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        nullptr,
        0,
        1,
        &deviceQueueCreateInfo,
        0,
        nullptr,
        0,
        nullptr,
        & requiredDeviceFeatures
    };

    result = vkCreateDevice(*pPhysicalDevice, &deviceCreateInfo, nullptr, &logicalDevice);
   
    vkEnumerateInstanceLayerProperties(&numLayers, nullptr);
    if (numLayers != 0)
    {
        instanceLayerProperties.resize(numLayers);
        vkEnumerateInstanceLayerProperties(&numLayers, instanceLayerProperties.data());
    }

    printf("\n\nVulkan instance layer properties");
    for (int i = 0; i < numLayers; i++)
    {
        printf("\nvulkan instance layerName = %s", instanceLayerProperties.at(i).layerName);
    }


    // device layer properties
    vkEnumerateDeviceLayerProperties(*pPhysicalDevice , &numLayers, nullptr);
    if (numLayers != 0)
    {
        instanceLayerProperties.resize(numLayers);
        vkEnumerateDeviceLayerProperties(*pPhysicalDevice , &numLayers, instanceLayerProperties.data());
    }

    printf("\n\nDevice layer instance properties");
    for (int i = 0; i < numLayers; i++)
    {
        printf("\nDevice layerName = %s", instanceLayerProperties.at(i).layerName);
    }

    // instance extension
    vkEnumerateInstanceExtensionProperties(nullptr, &numExtensions, nullptr);
    if (numExtensions != 0)
    {
        instanceExtensionProperties.resize(numExtensions);
        vkEnumerateInstanceExtensionProperties(nullptr, &numExtensions, instanceExtensionProperties.data());
    }

    printf("\n\nExtension layer instance properties");
    for (int i = 0; i < numExtensions; i++)
    {
        printf("\nExtension layerName = %s", instanceExtensionProperties.at(i).extensionName);
    }

    // device extension
    vkEnumerateDeviceExtensionProperties(*pPhysicalDevice, nullptr, &numExtensions, nullptr);
    if (numExtensions != 0)
    {
        instanceExtensionProperties.resize(numExtensions);
        vkEnumerateDeviceExtensionProperties(*pPhysicalDevice, nullptr, &numExtensions, instanceExtensionProperties.data());
    }

    printf("\n\nExtension layer device properties");
    for (int i = 0; i < numExtensions; i++)
    {
        printf("\nExtension layerName = %s", instanceExtensionProperties.at(i).extensionName);
    }

    result = vkDeviceWaitIdle(logicalDevice);
    printf("\nvkDeviceWaitIdle result = %d", result);
    PRINT_VULKAN_RESULT(result);

    vkDestroyDevice(logicalDevice, nullptr);
    vkDestroyInstance(vkInstance, nullptr);

    printf("\n\nExiting vulkan program\n");

    return 0;
}
