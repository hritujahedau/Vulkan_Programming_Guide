#include <iostream>

using namespace std;

class BitArray {
private:
	uint8_t* dataArray;
	int len;
	int actualSize;
public:
	BitArray(int size) {
		this->len = size;
		this->actualSize = (size / 8) * 8;
		this->dataArray = (uint8_t*)malloc(size/8);
	}

	inline int size() {
		return len;
	}

	bool operator[](unsigned int index) {
		if (index > len) {
			throw new out_of_range("Index out of range");
		}
		int i = 0;
		constexpr int MAX_SIZE = sizeof(unsigned char) * 8;
		index = index % MAX_SIZE;
		i = index / MAX_SIZE;

		return ((1 << index) & dataArray[i]);
	}

	void set(int index, bool value)
	{
		if (index > len) {
			throw new out_of_range("Index out of range");
		}
		int i = 0;
		constexpr int MAX_SIZE = sizeof(unsigned char) * 8;
		index = index % MAX_SIZE;
		i = index / MAX_SIZE;
		if (value) {
			dataArray[i] = dataArray[i] | (1 << index);
		}
		else {
			dataArray[i] = dataArray[i] & ~(1 << index);
		}
	}

	void resize(int newSize) {
		if (newSize < actualSize) {
			len = newSize;
			return;
		}
		this->len = newSize;
		this->actualSize = (newSize / 8) * 8;
		this->dataArray = (uint8_t*)realloc(this->dataArray, newSize / 8);
	}
};

int main()
{
	BitArray rainStatus(5);
	rainStatus.resize(10);
	char status = 'n';

	for (int i = 0; i < rainStatus.size(); i++)
	{
		cout << "Day "<<i+1<<": Did it rain 'y' or 'n' " << endl;
		cin >> status;
		rainStatus.set(i, status == 'y');
	}

	cout << "Rainy days: " << endl;
	for (int i = 0; i < rainStatus.size(); i++)
	{						
		if (rainStatus[i])
		{
			cout <<"Day " << i+1 << endl;
		}
	}

	cout << "\nNon rainy days: " << endl;
	for (int i = 0; i < rainStatus.size(); i++)
	{
		if (!rainStatus[i])
		{
			cout << "Day " << i + 1 << endl;
		}
	}
}
