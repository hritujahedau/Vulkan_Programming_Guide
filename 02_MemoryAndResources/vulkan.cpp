#include <stdio.h>
#include <vulkan\vulkan.h>

int main()
{
	VkResult vkRes;

	VkInstanceCreateInfo pCreateInfo{};
	VkApplicationInfo appInfo{};
	VkInstance instance{};
	
	VkPhysicalDevice physicalDevice{};
	uint32_t physicalDeviceCount = 1;

	VkDevice device{};
	VkDeviceCreateInfo deviceInfo{};
	
	
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pNext = nullptr;
	appInfo.pApplicationName = "Hrituja's vulkan program";
	appInfo.applicationVersion = 1;
	appInfo.apiVersion = VK_MAKE_VERSION(1.0, 0.0, 0.0);

	const char* layerNames = "VK_LAYER_KHRONOS_validation";

	pCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	pCreateInfo.pNext = nullptr;
	pCreateInfo.pApplicationInfo = &appInfo;
	pCreateInfo.enabledLayerCount = 1;
	pCreateInfo.ppEnabledLayerNames = &layerNames;
	pCreateInfo.enabledExtensionCount = 0;

	vkRes = vkCreateInstance(&pCreateInfo, nullptr, &instance);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateInfo");
		return 0;
	}

	// get physical device
	vkRes = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, &physicalDevice);
	// returns VK_INCOMPLETE because I have only for 1 device
	

	// create logical device
	VkDeviceQueueCreateInfo deviceQueueCreateInfo{};
	float f = 1.0;
	deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueCreateInfo.pNext = nullptr;
	deviceQueueCreateInfo.flags = 0;
	deviceQueueCreateInfo.queueCount = 1;
	deviceQueueCreateInfo.queueFamilyIndex = 0;
	deviceQueueCreateInfo.pQueuePriorities = &f;

	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceInfo.pNext = nullptr;
	deviceInfo.queueCreateInfoCount = 1;
	deviceInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
	deviceInfo.flags = 0;
	deviceInfo.enabledLayerCount = 0;
	deviceInfo.ppEnabledLayerNames = nullptr;
	deviceInfo.enabledExtensionCount = 0;
	deviceInfo.ppEnabledExtensionNames = nullptr;
	deviceInfo.pEnabledFeatures = nullptr;

	vkRes = vkCreateDevice(physicalDevice, &deviceInfo, nullptr, &device);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: VkCreateDevice");
		return 0;
	}

	static const VkBufferCreateInfo bufferCreateInfo =
	{
		VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		nullptr,
		0, 1024 * 1024,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT,
		VK_SHARING_MODE_EXCLUSIVE,
		0, nullptr
	};

	VkBuffer buffer = VK_NULL_HANDLE;
	vkRes = vkCreateBuffer(device, &bufferCreateInfo, nullptr, &buffer);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBuffer");
		return 0;
	}

	VkMemoryRequirements memoryReq =
	{
		1024 * 1024, 
		1024, 
		0
	};

	vkGetBufferMemoryRequirements(device, buffer, &memoryReq);

	VkImage image = VK_NULL_HANDLE;
	static const VkImageCreateInfo imageCreateInfo =
	{
		VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, nullptr,
		0,
		VK_IMAGE_TYPE_2D,
		VK_FORMAT_R8G8B8A8_UNORM,
		{1024, 1024, 1},
		10,
		1,
		VK_SAMPLE_COUNT_1_BIT,
		VK_IMAGE_TILING_OPTIMAL,
		VK_IMAGE_USAGE_SAMPLED_BIT,
		VK_SHARING_MODE_EXCLUSIVE,
		0,
		nullptr,
		VK_IMAGE_LAYOUT_UNDEFINED
	};

	vkRes = vkCreateImage(device, &imageCreateInfo, nullptr, &image);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateImage\n");
		return 0;
	}

	vkGetImageMemoryRequirements(device, image, &memoryReq);

	VkBufferViewCreateInfo bufferViewCreateInfo =
	{
			VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO,
			nullptr,
			0,
			buffer,
			VK_FORMAT_UNDEFINED,
			1024 
	};

	VkBufferView bufferView;
	vkRes = vkCreateBufferView(device, &bufferViewCreateInfo, nullptr, &bufferView);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBufferView\n");
		return 0;
	}

	// destroy 
	while(vkDeviceWaitIdle(device) != VK_SUCCESS);

	vkDestroyImage(device, image, nullptr);

	vkDestroyBuffer(device, buffer, nullptr);

	vkDestroyDevice(device, nullptr);

	vkDestroyInstance(instance, nullptr);

	return 0;
}
