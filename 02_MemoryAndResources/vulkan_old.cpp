#include <iostream>
#include <memory>
#include <vector>
#include <vulkan\vulkan.h>

using namespace std;

#define PRINT_ALLOCATOR_LOG 0

#define H_VK_SEPERATE_API_VERSION(version) \
        printf("%d.%d.%d.%d", VK_API_VERSION_VARIANT(version), VK_API_VERSION_MAJOR(version), \
        VK_API_VERSION_MINOR(version), VK_API_VERSION_PATCH(version));

#define PRINT_VULKAN_RESULT(result) ((result == VK_SUCCESS)? printf("\nSuccess: %s API executed successfully") : printf("\nFail: %s API executed unsuccessfully"))
#define ERR(function) vkres = function;\
if(vkres == VK_SUCCESS) { printf("successful: Function:"#function);} else {printf("failed: Function:"#function" with %d", vkres);}\
printf("\n");
#define PRINT_CALLING_STRING(s, ...) if(PRINT_ALLOCATOR_LOG) printf("Calling API "##s"\n", __VA_ARGS__);

class HostAllocator
{
private:
    static void* VKAPI_CALL AllocationCallback(
        void* pUserData,
        size_t size,
        size_t alignment,
        VkSystemAllocationScope allocationScope
    );

    static void* VKAPI_CALL ReallocationCallback(
        void* pUserData,
        void* pOriginalAllocation,
        size_t size,
        size_t alignment,
        VkSystemAllocationScope allocationScope
    );

    static void VKAPI_CALL FreeCallback(
        void*   pUserData,
        void*   pMemory
    );


    // our memeber allocatio, reallocation and free APIs
    void* Allocation(
        size_t size,
        size_t alignment
    );

    void* Reallocation(
        void* pOriginalAllocation,
        size_t size,
        size_t alignment
    );

    void Free(
        void* pMemory
    );

public:
    inline operator VkAllocationCallbacks() const
    {
        VkAllocationCallbacks result;
        result.pUserData = (void*)this;
        result.pfnAllocation = &AllocationCallback;
        result.pfnReallocation = &ReallocationCallback;
        result.pfnFree = &FreeCallback;
        result.pfnInternalAllocation = nullptr;
        result.pfnInternalFree = nullptr;

        return result;
    }
};

void* HostAllocator::Allocation(
    size_t size,
    size_t alignment
)
{
    PRINT_CALLING_STRING("HostAllocator::Allocation size = %zd", size);
    return _aligned_malloc(size, alignment);
}

void* HostAllocator::Reallocation(
    void* pOriginalAllocation,
    size_t size,
    size_t alignment
)
{
    PRINT_CALLING_STRING("HostAllocator::Reallocation size %zd", size);
    return _aligned_realloc(pOriginalAllocation, size, alignment);
}

void HostAllocator::Free(
    void* pMemory
)
{
    PRINT_CALLING_STRING("HostAllocator::Free");
    _aligned_free(pMemory);
}

void* VKAPI_CALL HostAllocator::AllocationCallback(
    void* pUserData,
    size_t size,
    size_t alignment,
    VkSystemAllocationScope allocationScope
)
{
    return static_cast<HostAllocator*>(pUserData)->Allocation(size, alignment);
}

void* VKAPI_CALL HostAllocator::ReallocationCallback(
    void* pUserData,
    void* pOriginalAllocation,
    size_t size,
    size_t alignment,
    VkSystemAllocationScope allocationScope
)
{
    return static_cast<HostAllocator*>(pUserData)->Reallocation(pOriginalAllocation, size, alignment);
}

void VKAPI_CALL HostAllocator::FreeCallback(
    void* pUserData,
    void* pMemory
)
{
    return static_cast<HostAllocator*>(pUserData)->Free(pMemory);
}

void main()
{
    // declarations
    // instance
    VkInstance instance;
    VkInstanceCreateInfo instanceCreateInfo{};
    VkResult vkres;
    HostAllocator hostAllocator;

    // physical device
    VkPhysicalDevice physicalDevice{};
    uint32_t physicalDeviceCount = 1;

    // logical device
    VkDevice device{};
    VkDeviceCreateInfo deviceCreateInfo{};

    // buffer
    VkBuffer buffer = VK_NULL_HANDLE;
    VkBufferCreateInfo bufferCreateInfo{};

    VkImage image = VK_NULL_HANDLE;

    // implementation
    // create instance
    const char* layerName = "VK_LAYER_KHRONOS_validation";

    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pNext = nullptr;
    instanceCreateInfo.flags = 0;
    instanceCreateInfo.enabledExtensionCount = 0;
    instanceCreateInfo.enabledLayerCount = 1;
    instanceCreateInfo.ppEnabledLayerNames = &layerName;
    instanceCreateInfo.pApplicationInfo = nullptr;

    VkAllocationCallbacks hostAllocatorCallback = (VkAllocationCallbacks)hostAllocator;

    ERR(vkCreateInstance(&instanceCreateInfo, &hostAllocatorCallback, &instance));

    // get physical device
    // returns VK_INCOMPLETE because I have only for 1 device
    vkres = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, &physicalDevice);
    
    // create logical device
    VkDeviceQueueCreateInfo deviceQueueCreateInfo{};
    float f = 1;

    deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    deviceQueueCreateInfo.pNext = nullptr;
    deviceQueueCreateInfo.flags = 0;
    deviceQueueCreateInfo.queueCount = 1;
    deviceQueueCreateInfo.queueFamilyIndex = 0;
    deviceQueueCreateInfo.pQueuePriorities = &f;

    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pNext = nullptr;
    deviceCreateInfo.enabledExtensionCount = 0;
    deviceCreateInfo.enabledLayerCount = 0;
    deviceCreateInfo.flags = 0;
    deviceCreateInfo.ppEnabledExtensionNames = nullptr;
    deviceCreateInfo.ppEnabledLayerNames = nullptr;
    deviceCreateInfo.queueCreateInfoCount = 1;
    deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
    deviceCreateInfo.pEnabledFeatures = nullptr;

    ERR(vkCreateDevice(physicalDevice, &deviceCreateInfo, &hostAllocatorCallback, &device));

    //create buffer
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.pNext = nullptr;
    bufferCreateInfo.flags = 0;
    bufferCreateInfo.size = 1024 * 1024;
    bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferCreateInfo.pQueueFamilyIndices = 0;
    bufferCreateInfo.pQueueFamilyIndices = nullptr;

    ERR(vkCreateBuffer(device, &bufferCreateInfo, &hostAllocatorCallback, &buffer));

    static const VkImageCreateInfo imageCreateInfo =
    {
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        nullptr,
        0,
        VK_IMAGE_TYPE_2D,
        VK_FORMAT_R8G8B8A8_UNORM,
        {1024, 1023, 1},
        10,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        nullptr,
        VK_IMAGE_LAYOUT_UNDEFINED
    };

    ERR(vkCreateImage(device, &imageCreateInfo, nullptr, &image));
    
}
