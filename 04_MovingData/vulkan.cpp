#include <iostream>
#include <vector>

#include <vulkan\vulkan.h>

using namespace std;

uint32_t ChooseHeapFromFlag(VkPhysicalDevice physicalDevice, const VkMemoryRequirements& memReq, VkMemoryPropertyFlagBits flag)
{
	VkPhysicalDeviceMemoryProperties deviceMemProp{};
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &deviceMemProp);

	uint32_t selectedType = ~0u;
	uint32_t memoryType;

	for (memoryType = 0; memoryType < 32; memoryType++)
	{
		if (memReq.memoryTypeBits & (1 << memoryType))
		{
			const VkMemoryType& type = deviceMemProp.memoryTypes[memoryType];
			if ((type.propertyFlags & flag) == flag)
			{
				selectedType = memoryType;
				break;
			}
		}
	}
	return selectedType;
}

void main()
{
	VkResult vkRes;

	VkInstance instance;
	VkInstanceCreateInfo instanceCI{};

	VkApplicationInfo applicationInfo{};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = "Hrituja's Vulkan code";
	applicationInfo.applicationVersion = 1;
	applicationInfo.pEngineName = "Unreal";
	applicationInfo.apiVersion = VK_VERSION_1_0;

	char* layerName = "VK_LAYER_KHRONOS_validation";
	instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCI.pApplicationInfo = &applicationInfo;
	instanceCI.ppEnabledLayerNames = &layerName;
	instanceCI.enabledLayerCount = 1;

	vkRes = vkCreateInstance(&instanceCI, nullptr, &instance);
	if (vkRes != VK_SUCCESS)
	{
		cout << "Failed: vkCreateInstance";
		return;
	}

	vector<VkPhysicalDevice> physicalDeviceVec;
	VkPhysicalDevice physicalDevice;
	uint32_t count;
	vkEnumeratePhysicalDevices(instance, &count, nullptr);

	physicalDeviceVec.resize(count);

	vkEnumeratePhysicalDevices(instance, &count, physicalDeviceVec.data());

	physicalDevice = physicalDeviceVec[0];

	uint32_t queueFamilyPropertyCount;
	uint32_t queueFamilyIndex = ~0u; // in VkQueueFamilyProperties's memebr index
	vector<VkQueueFamilyProperties> queueFamilyPropertiesVec;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);
	queueFamilyPropertiesVec.resize(queueFamilyPropertyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, queueFamilyPropertiesVec.data());

	for (int i = 0; i < queueFamilyPropertyCount; i++) {
		if (queueFamilyPropertiesVec[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			queueFamilyIndex = i;
			break;
		}
	}

	VkDevice device;

	VkDeviceQueueCreateInfo deviceQCI{};
	deviceQCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQCI.queueCount = 1;
	deviceQCI.queueFamilyIndex = queueFamilyIndex ; // ?
	float queuePriorities = 1.0;
	deviceQCI.pQueuePriorities = &queuePriorities; // ?
	
	VkPhysicalDeviceFeatures supportedfeatures{};
	VkPhysicalDeviceFeatures requiredfeatures{};

	vkGetPhysicalDeviceFeatures(physicalDevice, &supportedfeatures);
	requiredfeatures.multiDrawIndirect = supportedfeatures.multiDrawIndirect;
	requiredfeatures.tessellationShader = VK_TRUE;
	requiredfeatures.geometryShader = VK_TRUE;

	VkDeviceCreateInfo deviceCI{};
	deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCI.pQueueCreateInfos = &deviceQCI;
	deviceCI.queueCreateInfoCount = 1 ; // ? 
	deviceCI.pEnabledFeatures = &requiredfeatures;
	
	uint32_t data[8] = {1,2,3,4,5,6,7,8};

	vkCreateDevice(physicalDevice, &deviceCI, nullptr, &device);

	VkImage image, image2;
	static const VkImageCreateInfo imageCreateInfo =
    {
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        nullptr,
        0,
        VK_IMAGE_TYPE_2D,
        VK_FORMAT_R8G8B8A8_UNORM,
        {1024, 1023, 1},
        10,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        nullptr,
        VK_IMAGE_LAYOUT_UNDEFINED
    };

    vkRes = vkCreateImage(device, &imageCreateInfo, nullptr, &image);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkBeginCommandBuffer()");
		return;
	}

	VkBufferCreateInfo createBufferInfo{};
	createBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	createBufferInfo.queueFamilyIndexCount = 1;
	createBufferInfo.queueFamilyIndexCount = queueFamilyIndex;
	createBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;	
	createBufferInfo.size = sizeof(data);

	VkBuffer srcBuffer, dstBuffer;
	//src Buffer
	{
		createBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		vkCreateBuffer(device, &createBufferInfo, nullptr, &srcBuffer);
	}
	// dst buffer
	{
		createBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		vkCreateBuffer(device, &createBufferInfo, nullptr, &dstBuffer);
	}

	VkMemoryRequirements memReq;
	VkMemoryAllocateInfo memAllocInfo{};
	memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;

	VkDeviceMemory srcBufferMemory, dstBufferMemory;
	uint32_t memType;
	{
		vkGetBufferMemoryRequirements(device, srcBuffer, &memReq);

		memType = ChooseHeapFromFlag(physicalDevice, memReq, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		
		memAllocInfo.allocationSize = memReq.size;
		memAllocInfo.memoryTypeIndex = memType;

		vkAllocateMemory(device, &memAllocInfo, nullptr, &srcBufferMemory);
		vkBindBufferMemory(device, srcBuffer, srcBufferMemory, 0);
	}
	{
		vkGetBufferMemoryRequirements(device, dstBuffer, &memReq);

		memType = ChooseHeapFromFlag(physicalDevice, memReq, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

		memAllocInfo.allocationSize = memReq.size;
		memAllocInfo.memoryTypeIndex = memType;

		vkAllocateMemory(device, &memAllocInfo, nullptr, &dstBufferMemory);
		vkBindBufferMemory(device, dstBuffer, dstBufferMemory, 0);
	}	

	uint32_t *srcMapMemory, *dstMapMemory;

	vkMapMemory(device, srcBufferMemory, 0, VK_WHOLE_SIZE, 0, (void**)&srcMapMemory);
	vkMapMemory(device, dstBufferMemory, 0, VK_WHOLE_SIZE, 0, (void**)&dstMapMemory);

	memcpy(srcMapMemory, data, sizeof(data));
	// queues and commands to copy data from one buffer to another

	VkMappedMemoryRange mappedMemoryRange =
	{
		VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
		nullptr,
		srcBufferMemory,
		0,
		VK_WHOLE_SIZE
	};

	vkFlushMappedMemoryRanges(device, 1, &mappedMemoryRange);

	// create a queue
	VkQueue queue;
	vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue); 
	if (&queue == VK_NULL_HANDLE)
	{
		printf("Failed: to get a queue");
		return;
	}

	// command 
	VkCommandPool commandPool;
	VkCommandPoolCreateInfo commandPoolCreateInfo{};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = queueFamilyIndex;
	commandPoolCreateInfo.flags = 0;

	vkRes = vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkCreateCommandPool()");
		return;
	}

	VkCommandBuffer commandBuffer;
	VkCommandBufferAllocateInfo cmdBufferAI{};
	cmdBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBufferAI.commandPool = commandPool;
	cmdBufferAI.commandBufferCount = 1;
	cmdBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

	vkRes = vkAllocateCommandBuffers(device, &cmdBufferAI, &commandBuffer);
	if (vkRes != VK_SUCCESS) 
	{
		printf("Failed: vkAllocateCommandBuffers()");
		return;
	}

	// recording the command buffers
	VkCommandBufferBeginInfo cmdBufferBegineInfo{};
	cmdBufferBegineInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	
	vkRes = vkBeginCommandBuffer(commandBuffer, &cmdBufferBegineInfo);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkBeginCommandBuffer()");
		return;
	}

	VkBufferCopy bufferCpy{};
	bufferCpy.srcOffset = 0;
	bufferCpy.dstOffset = 0;
	bufferCpy.size = sizeof(data);

	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &bufferCpy);

	vkCmdFillBuffer(commandBuffer, dstBuffer, 0, 3 * sizeof(uint32_t), 10);

	uint32_t d[] = {100, 101, 102, 103};
	vkCmdUpdateBuffer(commandBuffer, dstBuffer, 4 * sizeof(uint32_t), bufferCpy.size - (4 * sizeof(uint32_t)), d);

	// clearing and filling image
	VkClearColorValue clearColorVal = {1.0, 2.0, 3.0, 4.0}; 

	VkImageSubresourceRange ranges{};
	ranges.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	ranges.baseMipLevel = 0;
	ranges.layerCount = 1;

	vkCmdClearColorImage(commandBuffer, image, VK_IMAGE_LAYOUT_GENERAL, &clearColorVal, 1, &ranges);

	VkClearDepthStencilValue depthStencilVal = {1, 1};

	vkCmdClearDepthStencilImage(commandBuffer, image, VK_IMAGE_LAYOUT_GENERAL, &depthStencilVal, 1, &ranges);

	VkImageSubresourceLayers imgSubresLayer{};
	imgSubresLayer.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imgSubresLayer.baseArrayLayer = 0;
	imgSubresLayer.layerCount = 1;
	imgSubresLayer.mipLevel = 1;

	VkBufferImageCopy region;
	VkImageCopy regionImg;

	vkCmdCopyBufferToImage(commandBuffer, srcBuffer, image, VK_IMAGE_LAYOUT_GENERAL, 1, &region);

	vkCmdCopyImageToBuffer(commandBuffer, image, VK_IMAGE_LAYOUT_GENERAL, dstBuffer, 1, &region);

	vkCmdCopyImage(commandBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image2, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &regionImg);

	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.pCommandBuffers = &commandBuffer;
	submitInfo.commandBufferCount = 1;

	vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

	vkRes = vkQueueWaitIdle(queue);
	if (vkRes != VK_SUCCESS)
	{
		printf("Failed: vkCreateBuffer");
		return ;
	}

	printf("\nsrc buffer data\n");
	for (int i = 0; i < size(data) / data[0]; i++)
	{
		printf(" %d = %d\n", i, srcMapMemory[i]);
	}

	printf("\ndst buffer data\n");
	for (int i = 0; i < size(data) / data[0]; i++)
	{
		printf(" %d = %d\n", i, dstMapMemory[i]);
	}

	vkUnmapMemory(device, srcBufferMemory);
	vkUnmapMemory(device, dstBufferMemory);

	vkFreeMemory(device, srcBufferMemory, nullptr);
	vkFreeMemory(device, dstBufferMemory, nullptr);
}

